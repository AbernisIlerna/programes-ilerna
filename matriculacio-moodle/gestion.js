// Clonamos el curso.
$(document).ready(function(){
	$('#boto').click(function(){

		// Comprobamos que los campos no esten vacios en caso de estarlo mostraremos un error.
		if($('#id_category').val()!="" && $('#name_category').val() != "" && $('#name_category_rp').val() != ""){ 
		
		// Mensaje de espera.
		$('#txtSelect').html("<p style='color: green; font-size: 25px;'>Clonando</p>");

		//Enviamos los datos al archivo php y mostramos lo que este nos devuelve.
		$.get('ws_clonar_categories.php', {'id_category' : $('#id_category').val(),'name_category' : $('#name_category').val(),'name_category_rp' : $('#name_category_rp').val()}, function(data){
			$('#txtSelect').html(data);
		});
		}
		else{ 
			$('#txtSelect').html("<p style='color: red; font-size: 25px;'> <strong> Rellene todos los campos </strong> </p>");
		}
	});
});

//Matriculamos ciclos
$(document).ready(function(){
	$('#btn_matricular').click(function(){

		// Comprobamos que los campos no esten vacios en caso de estarlo mostraremos un error.
		if($('#codigo_matricular').val()!=""){

		// Mensaje de espera. 
		$('#txtSelect').html("<p style='color: green; font-size: 25px;'>Matriculando</p>");

		//Enviamos los datos al archivo php y mostramos lo que este nos devuelve.
		$.get('ws_import_users_from_softaula.php', {'codigo_grupo' : $('#codigo_matricular').val()}, function(data){
			$('#txtAlumnos').html(data);
				// Matriculamos los Ciclos
				$.get('ws_matriculacions_from_softaula_V2.php', {'codigo_matricular' : $('#codigo_matricular').val()}, function(data){
				$('#txtSelect').html(data);
				});
		});
		}
		else{ 
			$('#txtSelect').html("<p style='color: red; font-size: 25px;'> <strong> Rellene todos los campos </strong> </p>");
		} 
		 
	});
});

// Matriculamos curso
$(document).ready(function(){
	$('#btn_matricular_curso').click(function(){ 

		// Comprobamos que los campos no esten vacios en caso de estarlo mostraremos un error.
		if($('#codigo_matricular_curso').val()!=""){

		// Mensaje de espera. 
		$('#txtSelect').html("<p style='color: green; font-size: 25px;'>Importando</p>");

		//Enviamos los datos al archivo php y mostramos lo que este nos devuelve.
		$.get('ws_import_users_from_softaula.php', {'codigo_grupo' : $('#codigo_matricular_curso').val()}, function(data){
			$('#txtAlumnos').html(data);

			// Matriculamos los Cursos
			$.get('ws_matriculacions_from_softaula_V2_Cursos.php', {'codigo_matricular' : $('#codigo_matricular_curso').val()}, function(data){
			$('#txtSelect').html(data);
			});
		});
		}
		else{ 
			$('#txtSelect').html("<p style='color: red; font-size: 25px;'> <strong> Rellene todos los campos </strong> </p>");
		}
	});
});
// Apartado para hacer aparecer el formulario de clonar
$(document).ready(function(){
	$('#clonar').click(function(){

		// Hacemos aparecer y desaparecer los diferentes apartados
		$('#apartado-clonar').css("display","block"); 
		$('#matricular_actividad_curso').css("display","none");
		$('#matricular').css("display","none");

		// Quitamos o ponemos la clase active en los botones
		$('#clonar').addClass("active"); 
		$('#matricular_actividad').removeClass("active");
		$('#matricular_grupos').removeClass("active");

		// Vaciamos el apartado que tiene los datos que devuelven las consultas
		$('#txtSelect').html("");
		$('#txtAlumnos').html('');

		// Vaciamos los diferentes inputs que existen. 
		$('#id_category').val('');
		$('#name_category').val('');
		$('#name_category_rp').val('');
		$('#codigo_matricular').val('');
		$('#codigo_matricular_curso').val('');
	});
});


//Apartado para hacer que aparezca el apartado de matricular
$(document).ready(function(){
	$('#matricular_grupos').click(function(){

		// Hacemos aparecer y desaparecer los diferentes apartados 
		$('#apartado-clonar').css("display","none");
		$('#matricular_actividad_curso').css("display","none");
		$('#matricular').css("display","block");

		// Quitamos o ponemos la clase active en los botones
		$('#clonar').removeClass("active"); 
		$('#matricular_actividad').removeClass("active");
		$('#matricular_grupos').addClass("active");

		// Vaciamos el apartado que tiene los datos que devuelven las consultas
		$('#txtSelect').html("");
		$('#txtAlumnos').html('');

		// Vaciamos los diferentes inputs que existen.
		$('#id_category').val(''); 
		$('#name_category').val('');
		$('#name_category_rp').val('');
		$('#codigo_matricular').val('');
		$('#codigo_matricular_curso').val('');
	});
});


//Apartado para hacer que aparezca el apartado de matricular curso
$(document).ready(function(){
	$('#matricular_actividad').click(function(){

		// Hacemos aparecer y desaparecer los diferentes apartados 
		$('#apartado-clonar').css("display","none");
		$('#matricular').css("display","none");
		$('#matricular_actividad_curso').css("display","block");

		// Quitamos o ponemos la clase active en los botones
		$('#clonar').removeClass("active"); 
		$('#matricular_grupos').removeClass("active");
		$('#matricular_actividad').addClass("active");

		// Vaciamos el apartado que tiene los datos que devuelven las consultas
		$('#txtSelect').html("");
		$('#txtAlumnos').html('');

		// Vaciamos los diferentes inputs que existen.
		$('#id_category').val('');
		$('#name_category').val('');
		$('#name_category_rp').val('');
		$('#codigo_matricular').val('');
		$('#codigo_matricular_curso').val('');
	});
});