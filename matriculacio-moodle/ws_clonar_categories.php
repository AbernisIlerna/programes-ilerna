<?php
/* Clona els cursos de les categories seleccionades amb activitats, blocs i altres configuracions excepte matriculacions notes i altra info relacionada amb l'usuari */

// Recogemos los datos embiados por el js.
$id_category=$_GET["id_category"];
$name_category=$_GET["name_category"];
$name_category_rp=$_GET["name_category_rp"];


/*
$short_name_category=$_GET["short_name_category"];
$short_name_category_rp=$_GET["short_name_category_rp"];
 */


//ID categories a clonar. 
//Separamos la variable por comas y la metemos dentro de un array
$array_idCategories = explode(",", $id_category);

/* Clona els cursos de les categories seleccionades amb activitats, blocs i altres configuracions excepte matriculacions notes i altra info relacionada amb l'usuari */

require_once('curl.php');

ini_set('max_execution_time', 900);
//header('Content-Type: text/html; charset=UTF-8'); 
header('Content-Type: text/plain ; charset=UTF-8');

/// SETUP - NEED TO BE CHANGED
$wsusername = "webserviceauth";
$wspassword = "4B2x9@tD1!";
$domainname = 'http://campus.ilerna.com';

//BBDD produccio
$servidor = "130.211.56.152:3306";

//$servidor = "130.211.56.152";
$usuari = "usermoodle45";
$password = "16180moodle45";
$dbname = "bdmoodle45";


// REST RETURNED VALUES FORMAT
$restformat = 'xml'; //Also possible in Moodle 2.2 and later: 'json'
                     //Setting it to 'json' will fail all calls on earlier Moodle version


$lb = "\r"; //line break
					 


//$array_idCategories = array("129");


$con=mysqli_connect($servidor,$usuari,$password,$dbname);
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

foreach ($array_idCategories as &$valor) {
    $idCurrentCategory = $valor;
	
	//Duplica categoria
	$functionname = 'core_course_create_categories';
	$parentCategory = 0; // ID de la categoria pare destí
	$descriptionFormat = "1";
	$theme="";

	$resultCategory = mysqli_query($con,"SELECT cc.id, cc.name, cc.parent FROM mdl_course_categories cc WHERE cc.id = ".$idCurrentCategory);
	

echo $idCurrentCategory."     ";


	$i=0;
	while($row2 = mysqli_fetch_array($resultCategory)){
		$categoryName = str_replace($name_category,$name_category_rp, $row2['name']); // ============ aqui canviar nom
		unset($categories);
		
		/// PARAMETERS
		$categories[$i]['name']=iconv(mb_detect_encoding($categoryName, mb_detect_order(), true), "UTF-8", $categoryName);
		$categories[$i]['parent']=$parentCategory;
		$categories[$i]['idnumber']= "";
		$categories[$i]['description']= "";
		$categories[$i]['descriptionformat']=$descriptionFormat;
		$categories[$i]['theme']= $theme;
		
		
		echo $lb.$row2['id'].$lb;
		echo $categories[$i]['name'];
		echo $categories[$i]['parent'];
		
		unset($params);
		
		$params['categories']=$categories;
		///*
		/// REST CALL
		//header('Content-Type: text/plain ; charset=UTF-8');
		$serverurl = $domainname . '/webservice/rest/simpleserver.php'. '?wsusername=' . $wsusername . '&wspassword=' . $wspassword . '&wsfunction='.$functionname;
		//require_once('../curl.php');
		$curl = new curl;
		//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
		//$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
		$restformat = '&moodlewsrestformat=json';
		
		unset($resp);
		
		$resp = $curl->post($serverurl . $restformat, $params);
		//print_r($resp).$lb;
		//*/
		$xml=null;
		$xml=simplexml_load_string($resp);
		
		$idNewCategoryString = $xml->MULTIPLE->SINGLE->KEY[0]->VALUE;
		$idNewCategory = intval($idNewCategoryString);
		
		echo "id categoria nova ".$idNewCategory;
		
		//$i++;
		
	}


	//Duplica els cursos a la categoria acabada de crear
	$functionname = 'core_course_duplicate_course';

	$resultCourses = mysqli_query($con,"SELECT id, fullname, shortname,idnumber FROM mdl_course WHERE category = ".$idCurrentCategory);

	$j=0;
	
	while($row = mysqli_fetch_array($resultCourses)){
///*	
	
		/// PARAMETERS
		$params[$j] = array(
		'courseid' => $row['id'], // Course to be duplicated idnumber // CHANGE IT FOR AN EXISTING COURSE ID
		//'fullname' => $row['fullname'], // New course full name
		'fullname' => iconv(mb_detect_encoding($row['fullname'], mb_detect_order(), true), "UTF-8", $row['fullname']), // New course full name
		'shortname' => str_replace($name_category,$name_category_rp, $row['shortname']), // New course shortname ========== aqui canviar el nom curt
		'categoryid' => $idNewCategory, // New course category id
	//'categoryid' => 311, // New course category id
		'visible' => 1, // Make the course visible after duplicating
		'options' => array(
			array('name'=>'activities', 'value'=>1), 
			array('name'=>'blocks', 'value'=>1), 
			array('name'=>'filters', 'value'=>1), 
			array('name'=>'users', 'value'=>0),
			array('name'=>'role_assignments', 'value'=>0),
			array('name'=>'comments', 'value'=>0),
			array('name'=>'userscompletion', 'value'=>0),
			array('name'=>'logs', 'value'=>0),
			array('name'=>'grade_histories', 'value'=>0))
		);
//*/	


			//Miramos si el curso que intentamos clonar ya existe.
			$existCourses = mysqli_query($con,"SELECT count(id) FROM mdl_course WHERE shortname = '".$params[$j]['shortname']."'");  

			$rowexiste = mysqli_fetch_array($existCourses); 

			//Si existe mostramos error y continuamos al siguiente.
			if($rowexiste[0] > 0){
				echo "<br> <p style='color: red;'> No se puede crear el curso: </p>";
				echo "<p style='color: red;'> El nombre corto "."<strong> Shorname: </strong> ".$params[$j]['shortname'].$lb." del curso "."<strong> Fullname: </strong>".$params[$j]['fullname'].$lb." existe. </p>";
			}
			else{
			echo "<br> <strong> ID Curs: </strong> ".$params[$j]['courseid'].$lb;
			echo "<strong> Fullname: </strong> ".$params[$j]['fullname'].$lb;
			echo "<strong> Shortname: </strong> ".$params[$j]['shortname'].$lb;
			echo "<strong> Categoria destí: </strong> ".$params[$j]['categoryid'].$lb;
			echo "<strong> Visibilitat: </strong> ".$params[$j]['visible'].$lb.$lb;
			echo "<strong> Visibilitat: </strong> ".$row[3].$lb.$lb;  
			
			}

			/// REST CALL
			//header('Content-Type: text/plain ; charset=UTF-8');
			$serverurl = $domainname . '/webservice/rest/simpleserver.php'. '?wsusername=' . $wsusername . '&wspassword=' . $wspassword . '&wsfunction='.$functionname;
			//require_once('../curl.php');
			$curl = new curl;
			//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
			//$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
			$restformat = '&moodlewsrestformat=json';
			$resp = $curl->post($serverurl . $restformat, $params[$j]);
			//print_r($resp);
 			
 			// Apartado donde introduce el ID del curso en los modulos que se clonaron. Cambiando el año del ID curso. 
 			// Aconseguimos el ID del curso y cambiamos el año.
			$IdCurso = str_replace($name_category,$name_category_rp, $row[3]);
			// Realizamos el update necesario para aplicar el ID del curso que deseamos en el modulo que le corresponda. Se busca el modulo mediante su nombre corto.
			$resultadoUpdate = mysqli_query($con,'UPDATE mdl_course SET idnumber = "'.$IdCurso.'" WHERE shortname = "'.$params[$j]['shortname'].'"');
			}
			$j++;

} 


mysqli_close($con);
 
?>