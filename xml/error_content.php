<?php
function contigutErrors($errors){
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico?1457949818">

    <title>Ilerna</title>

     <link href="./themes/ilerna-online/css/avenir-lt-std/styles.css" rel="stylesheet">
     <link href="./style.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="./dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="./assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

     
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./assets/js/ie-emulation-modes-warning.js"></script>
    <script src="./index.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="background: 50% 50% / cover no-repeat fixed;" class="fondoBody"> 
  
    <nav class="navbar navbar-inverse navbar-fixed-top headerFondo navLanding">

      <div class="container">

        <div class="navbar-header">
          <img src="./img/logos/perfil2.png" />
        </div>
          <!--
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
        
    <!--
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse --> 
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action 
    <div class="jumbotron">
      <div class="container">
        <h1>Benvinguts a Ilerna</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p> ->
      </div>
    </div>
  -->

    <div class="container panelPrincipal">
      <!-- Example row of columns -->

            
      <div style="margin-top:20px;">
        <div class="row">
          <div class="panel-group ">
            <div class="col-md-2">
            </div> 
            <div class="col-md-8">
              <div class="panel fondoPanelInici clearfix">
                <div class="panel-heading text-center">                
                  <img class="img-responsive center-block logoCaixa" src="./img/logos/lleida.png" alt="Lleida">                 
                </div>
                <div class="panel-body">
                  <div id="intro-codi"> 
                    <div class="row">
                      <?php
                       if(isset($errors))echo $errors; 
                      ?>   
                    </div>                  
                  </div>
                  <br/>  
                </div>
                 
              </div>          
            </div>
            <div class="clearfix visible-sm"></div> 
            <div class="col-md-2">
            </div> 
            <div class="clearfix visible-sm"></div> 
          </div>
        </div>   
      </div>
   
  
    <!--
    <footer class>
        <p>&copy; 2016 Ilerna, Inc.</p>
      </footer>
      -->

    </div> 

    <!-- /container -->
    
    
    
     

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>

<script>    
    // When the document is loaded...
    $(document).ready(function()
    {     
        
        // Validacio formulari
        $('form#docgenerate').submit(function (e) {

            
            // Guardem el valors dels camps
            var codi = $.trim($('#tipodoc').val());
            var tipdoc = $.trim($('#grup').val());            
            var correcte = true; 
            var errors = [2];
            // Comprovem que els valors siguin correctes
            if (codi  == '0') {    //Quan hi ha un error al formulari             
                $("#error1").removeClass('hidden');
                //$("#name").addClass('error');     
                errors[0]="1";
            }
            else                   //Quan tot està correcte
            {                
                $("#error1").addClass('hidden');     
                //$("#name").removeClass('error');
                errors[0]="0";
            }

            if(tipdoc  == '')    //Quan hi ha un error al formulari 
            {              
                $("#error2").removeClass('hidden');                
                //$("#email").addClass('error');  
                errors[1]="1";
            }
            else                    //Quan tot està correcte
            { 
                $("#error2").addClass('hidden');                 
                //$("#email").removeClass('error');
                errors[1]="0";
            }

            //COMPROVACIÓ DE SI HI HA ERRORS
            var isGood=true;
            for (i = 0; i < errors.length; i++) 
            { 
                if(errors[i]==1) isGood=false;
            }

            if(!isGood){            //Quan hi ha algun error al formulari
                //alert("errors");
                e.preventDefault();
                //$(document).resize();
            }
            else{                   //Quan tot està correcte
                $("input#enviar").prop('value', "Esperi sisplau...");
                $("input#enviar").prop('disabled', true);              
            }  
        }); 
    });

</script> 
</html> 
 <?php
}
 ?>