<?php
include "error_content.php";
//CLASSE ALUMNE
class Alumne{
    public $nparticipant;
    public $ccodcli;
    public $info;

    function __construct($codi, $nom, $num){
        $this->ccodcli = $codi;
        $this->info = $nom;
        $this->nparticipant = $num;
    }
} 

//INICI PROGRAMA
if(!isset($_POST["grup"]) && empty($_POST["grup"]) && !isset($_POST["tipodoc"]) && empty($_POST["tipodoc"]) && !isset($_GET["download"])){
    header("Location: index.html");
}
else{
    //SI L'USUARI VOL FORÇAR LA DESCARREGA DEL FITXER
    if(isset($_GET["download"]) && !empty($_GET["download"]) && isset($_GET["codi"]) && !empty($_GET["codi"]) && isset($_GET["doc"]) && !empty($_GET["doc"])){
        $codigrup = $_GET["codi"];
        $tipodoc = $_GET["doc"];
        $download = 1;
    }
    else{
        $codigrup = $_POST["grup"];
        $tipodoc = $_POST["tipodoc"];
        $download = 0;
    } 

    switch ($tipodoc) {
        case '1':
            getTripartitaGestioParticipantsXml($download, $codigrup);
            break; 

        case '11':
            getTripartitaFinalitzacioGrupXml($download, $codigrup, '1'); // PEL ROL ORGANITZADORES
            break; 

        case '111':
            getTripartitaFinalitzacioGrupXml($download, $codigrup, '2'); // PEL ROL BONIFICADA
            break;

        case '2':
            getConforcatGestioParticipantsXml($download, $codigrup);
            break; 

        case '3':
            getConforcatCertificacioParticipantsXml($download, $codigrup);
            break; 
    } 
    
}



/*
* Function to get all data from specific group to create the TRIPARTITA XML
*/
function getTripartitaGestioParticipantsXml($download, $codigrup)
{   
    //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();
     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");
    $sql ="SELECT distinct a.CDNICIF, a.tipodoc, a.sapellidos, a.snombre, a.COBSCLI, a.idsexo, DATE_FORMAT(a.nacimiento, '%d/%m/%Y') FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 order by a.sapellidos";
    $result = mysqli_query($db, $sql);

    if($result->num_rows == 0){
        header("Location: index.php");
    }


    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors = new ArrayObject();    
    //CREACIÓ DOCUMENT XML
    $xml = new SimpleXMLElement('<participantes xmlns="http://www.fundaciontripartita.es/schemas"/>');
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($result,MYSQLI_NUM)) 
    {        
        //COM NO TENIM ELS COGNOMS SEPARATS, ELS SEPAREM A PARTIR D'UN ESPAI
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]."<br/>"; 
        if( count($row) == 7)
        {      
            //COMPROVACIÓ ERRORS SI ALGUN DELS CAMPS NO TE VALOR
            if($row[0] =="" || $row[1]=="" || $row[3]=="" || $row[4]=="" || $row[5]<1 || $row[6]==""){
                //COMPROVACIÓ DE DADES UNA PER UNA PER DESPRÉS MOSTRAR QUINA ÉS LA QUE FALTA
                $dadesQueFalta ='';
                if($row[0] ==""){
                    $dadesQueFalta .= '<p>- DNI</p>';
                }
                if($row[2]==""){
                    $dadesQueFalta .= '<p>- COGNOMS</p>';
                }
                if($row[3]==""){
                    $dadesQueFalta .= '<p>- NOM</p>';
                }
                if($row[4]==""){
                    $dadesQueFalta .= '<p>- NUM SS</p>';
                }
                if($row[5]<1){
                   $dadesQueFalta .= '<p>- SEXE</p>';
                }
                if($row[6]==""){    
                    $dadesQueFalta .= '<p>- DATA NAIXEMENT</p>';
                }
                //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
                $alumn = new Alumne($row[0], $dadesQueFalta, $contador_participants); 
                //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
                $llista_errors->append($alumn);   
            } 
            $participante = $xml->addChild('participante');
            $participante->addChild('D_DOCUMENTO', $row[0]); //NUMERO DNI O PASSAPORT        
            switch ($row[1]) {
                case '0':
                    $participante->addChild('N_TIPO_DOCUMENTO', '10'); //TIPUS DE DOCUMMENT -> DNI
                    break;
                case '1':
                   $participante->addChild('N_TIPO_DOCUMENTO', '60'); //TIPUS DE DOCUMMENT -> NIE
                    break; 
            } 
            if (strpos($row[2], ' ')) {
                $cognoms = explode(" ", $row[2]);                
                $cognom2 = "";
                for ($x = 1; $x < count($cognoms); $x++) {
                    $cognom2.=$cognoms[$x];
                }                  
                $participante->addChild('D_APELLIDO1', $cognoms[0]); //COGNOMS
                $participante->addChild('D_APELLIDO2', $cognom2); //COGNOMS                 
            }
            else{
                $participante->addChild('D_APELLIDO1', $row[2]); //COGNOMS
                $participante->addChild('D_APELLIDO2', ''); //COGNOMS
            }
            $participante->addChild('D_NOMBRE', $row[3]); //NOM
            $participante->addChild('D_NISS', $row[4]); //NUMERO SEGURETAT SOCIAL

            //SEXE
            switch ($row[5]) {
                case '2':
                    $participante->addChild('B_SEXO', 'F');//SEXE
                    break;  
                case '1':
                    $participante->addChild('B_SEXO', 'M');//SEXE
                    break;    
                default:
                    $participante->addChild('B_SEXO'); //ERROR DADA
                    break; 
            } 
            $participante->addChild('F_NACIMIENTO', $row[6]); //DATA NAIXEMENT            


        }
        $contador_participants ++;
    }
    
    if($download){         
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="tripartita_'.$codigrup.'.xml"'); 
        print($xml->asXML());
    }
    else{
        if(count($llista_errors)>0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            foreach($llista_errors as $key=>$value) {
                if($index%2==0) {
                    $llist_alumn .= '<tr class="danger"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';   
                }
                else{
                    $llist_alumn .= '<tr style="background-color: darkgray;"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';  
                }  
            
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="3">LLISTA DE PARTICIPANTS AMB ERROR DE DADES</th>
                    </tr>
                  </thead>
                  <thead>
                    <tr> 
                      <th>Nº PARTICIPANT</th>
                      <th>DNI ALUMNE</th>
                      <th>DADES QUE FALTEN</th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                    </div>
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                    </div>
                </div>
              </div>
            </div>';   
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            header('Content-type: text/xml');
            header('Content-Disposition: attachment; filename="tripartita_'.$codigrup.'.xml"'); 
            print($xml->asXML());
        }

    }

    //free result set 
    mysqli_free_result($result); 
     //close connection 
    mysqli_close($db); 
    
} 

/*
* Function to get all data from specific group to create a XML
* In this case, because of the imposibility to codify to UTF-16LE the xml I dicided to create the xml
* manualy, adding the content in a string and then writting it to a file
* However, I left the old code commented
*/
function getTripartitaFinalitzacioGrupXml($download, $codigrup, $tipusRol)
{   
    //OBTENCIÓ DE LES DADES DE L´ALUMNE 
    if($tipusRol=='1'){ //ROL ORGANITZADORA
        $sqlGeneral ="SELECT DISTINCT SUBSTRING(g.`Descripcion`, 3, 5),SUBSTRING(g.`Descripcion`, 9, 10),a.CDNICIF as 'DNI',a.tipodoc,a.sapellidos as 'COGNOMS',a.snombre as'NOM',a.COBSCLI as 'NUM SS',cl.CDNICIF as 'CIF',ad.cuenta_cotizacion_empresa as 'CUENTA COTIZACION',DATE_FORMAT(a.nacimiento, '%d/%m/%Y') as 'DATA NAIXEMENT',a.email as 'EMAIL',a.idsexo as 'SEXO' ,if(a.CTFO1CLI != '', a.CTFO1CLI, a.movil) as 'Movil',ad.discapacidad as 'Discapacitat',round(if(ad.`categoria profesional`>0, ad.`categoria profesional`, catprof.id)) as 'CATEGORIA',round(if(ad.`grupo profesional` >0, ad.`grupo profesional`, gprof.id)) as 'GRUPO PROFESIONAL',round(if(ad.estudis>0, ad.`estudis`, est.`id`)) as 'ESTUDIS' FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.alumnos_tab_ad AS  ad ON ad.`CCODCLI` = a.`CCODCLI` LEFT JOIN softaula.`clientes` as cl on cl.ccodcli =  mt.`FacturarA` LEFT JOIN softaula.`clientes_tab_ad` as clad on clad.ccodcli =  cl.ccodcli LEFT JOIN softaula.categorias_profesionales as catprof on catprof.valor = ad.`categoria profesional` LEFT JOIN softaula.grupo_profesional as gprof on gprof.valor = ad.`grupo profesional` LEFT JOIN softaula.estudis_tripartita AS est ON est.`Valor` = ad.`estudis_tripartita` WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 ORDER BY a.sapellidos";
    }else{ //ROL TRAMITADORA
        $sqlGeneral ="SELECT DISTINCT LEFT(RIGHT(g.`Descripcion`, 5), 2) AS 'N1', RIGHT(g.`Descripcion`, 2) AS 'N2', a.CDNICIF as 'DNI',a.tipodoc,a.sapellidos as 'COGNOMS',a.snombre as'NOM',a.COBSCLI as 'NUM SS',cl.CDNICIF as 'CIF',ad.cuenta_cotizacion_empresa as 'CUENTA COTIZACION',DATE_FORMAT(a.nacimiento, '%d/%m/%Y') as 'DATA NAIXEMENT',a.email as 'EMAIL',a.idsexo as 'SEXO' ,if(a.CTFO1CLI != '', a.CTFO1CLI, a.movil) as 'Movil',ad.discapacidad as 'Discapacitat',round(if(ad.`categoria profesional`>0, ad.`categoria profesional`, catprof.id)) as 'CATEGORIA',round(if(ad.`grupo profesional` >0, ad.`grupo profesional`, gprof.id)) as 'GRUPO PROFESIONAL',round(if(ad.estudis>0, ad.`estudis`, est.`id`)) as 'ESTUDIS' FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.alumnos_tab_ad AS  ad ON ad.`CCODCLI` = a.`CCODCLI` LEFT JOIN softaula.`clientes` as cl on cl.ccodcli =  mt.`FacturarA` LEFT JOIN softaula.`clientes_tab_ad` as clad on clad.ccodcli =  cl.ccodcli LEFT JOIN softaula.categorias_profesionales as catprof on catprof.valor = ad.`categoria profesional` LEFT JOIN softaula.grupo_profesional as gprof on gprof.valor = ad.`grupo profesional` LEFT JOIN softaula.estudis_tripartita AS est ON est.`Valor` = ad.`estudis_tripartita` WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 ORDER BY a.sapellidos";
    }
    

    
    //OBTENCIÓ DE LES DADES DELS COSTOS PER LES EMPRESES
    $sqlCostos ="SELECT DISTINCT cl.`CDNICIF` AS 'DNI', gad.`costos directes` AS 'COSTOS DIRECTES', gad.`costos indirectes` AS 'COSTOS INDIRECTES',gad.`costos organitzacio` AS 'COSTOS ORGANITZACIO', gad.`costos salarials` AS 'COSTOS SALARIAL' FROM softaula.matriculat AS mt LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.`clientes` AS cl ON cl.ccodcli =  mt.`FacturarA` LEFT JOIN softaula.grupos_tab_ad AS gad ON gad.codigogrupo = g.codigogrupo WHERE gad.`costos directes` > 0 AND g.codigogrupo='".$codigrup."' AND cl.`CDNICIF` IS NOT NULL";

     //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();

    $expedient ='';

     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");    
    $resultGeneral = mysqli_query($db, $sqlGeneral);  
    $resultCostos = mysqli_query($db, $sqlCostos);  

    if($resultGeneral->num_rows == 0) {// || $resultFormadors->num_rows == 0){
        header("Location: index.php");
    }

    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors_participants = new ArrayObject();   
    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE FORMADOR
    $llista_errors_costos = new ArrayObject();   
    //String on guardar els errors del grup
    $errors_grup = '' ;
    
    $string = '';

    //$diccionari =['', '', 'DNI'];
                         
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($resultGeneral,MYSQLI_ASSOC)) {
        $dadesQueFalta = '';
        $faltaDades = false;
        //$i=0;
        $arrayDadesNoObligatories = array('Discapacitat');
        foreach($row as $key=>$value) {
            //echo("key --> ".$key).'</BR>';
            if($key=='SEXO' && $value<1){
                $faltaDades = true;
                $dadesQueFalta .= '<p>- '.$key.' </p>';
            }
            else{
                if($value == '' && !in_array($key, $arrayDadesNoObligatories)){
                    //echo $i.'- '.$key.'</br>';
                    $faltaDades = true;
                    $dadesQueFalta .= '<p>- '.$key.' </p>';
                }   
            }     
            //$i++;
        }

        if($faltaDades){
            //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
            $alumn = new Alumne($row['DNI'], $dadesQueFalta, $contador_participants); 
            //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
            $llista_errors_participants->append($alumn);
        }
        $contador_participants ++;

    }
    $contador_participants = 1;
    $resultGeneral = mysqli_query($db, $sqlGeneral); 
    while ($row=mysqli_fetch_array($resultGeneral,MYSQLI_NUM)) 
    {    
        //ARRAY QUE GUARDA LA POSICIO DELS CAMPS NO OBLIGATORIS EN LA CONSULTA SQL PRINCIPAL PER AIXÍ COMPROVAR QUE SI FALTEN DADES SIGUIN DE LES QUE SON OBLIGATORIES
        //$arrayDadesNoObligatories = array();
        //COMPROVACIÓ ERRORS SI ALGUN DELS CAMPS NO TE VALOR
        /*
        for ($index = 0; $index < count($row); $index++) { 
                 if($row[$index] == '' || $row[$index] == null)//Si algun dels camps de
                 {
                    //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
                    $alumn = new Alumne($row[2], $row[5]." ".$row[4], $contador_participants); 
                    //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
                    $llista_errors_participants->append($alumn);
                    break;
                 }  
        } 
        */  
        
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]." -- ".$row[7]." -- ".$row[8]." -- ".$row[9]." -- ".$row[10]." -- ".$row[11]." -- ".$row[12]." -- ".$row[13]." -- ".$row[14]."<br/>"; 
        if(count($row) == 17)
        {   
            //Quan és el primer participant creem les linees que només apareixen un cop
            if($contador_participants == 1)
            {
                //CREACIÓ DOCUMENT XML A DINS D'UNA STRING PER DESPRÉS IMPRIMIR-LA    
                $string = '<grupos xmlns="http://www.fundaciontripartita.es/schemas">'.PHP_EOL.'<grupo>'.PHP_EOL;
                //EXPEDIENT
                if($row[0] != ''){
                    $string .= '<idAccion>'.$row[0].'</idAccion>'.PHP_EOL; 
                }else{
                    $string .= '<idAccion/>'.PHP_EOL;
                }
                if($row[1] != ''){
                    $string .= '<idGrupo>'.$row[1].'</idGrupo>'.PHP_EOL; 
                }else{
                    $string .= '<idGrupo/>'.PHP_EOL;
                }
                $string .='<participantes>'.PHP_EOL; 
            } 
                //INICI PARTICIPANTS
            $string .= '<participante>'.PHP_EOL;
            //NIF PARTICIPANT
            $row[2] != '' ? $string .= '<nif>'.$row[2].'</nif>'.PHP_EOL :  $string .= '<nif/>'.PHP_EOL; 
            //TIPO DE DOCUMENTO
            switch ($row[3]) {
                case '0':
                    $string .= '<N_TIPO_DOCUMENTO>10</N_TIPO_DOCUMENTO>'.PHP_EOL; //TIPUS DE DOCUMMENT -> DNI
                    break;
                case '1':
                   $string .= '<N_TIPO_DOCUMENTO>60</N_TIPO_DOCUMENTO>'.PHP_EOL; //TIPUS DE DOCUMMENT -> NIE
                    break; 
                case '':
                   $string .= '<N_TIPO_DOCUMENTO/>'.PHP_EOL; //ERROR
                    break; 
            } 
            //NOM PARTICIPANT
            $row[5] != '' ? $string .= '<nombre>'.$row[5].'</nombre>'.PHP_EOL :  $string .= '<nombre/>'.PHP_EOL;
            //COGNOM PARTICIPANT
            if (strpos($row[4], ' ')) {
                $cognoms = explode(" ", $row[4]);                
                $cognom2 = "";
                for ($x = 1; $x < count($cognoms); $x++) {
                    $cognom2.=$cognoms[$x];
                }      
                $string .= '<primerApellido>'.$cognoms[0].'</primerApellido>'.PHP_EOL;            
                $string .= '<segundoApellido>'.$cognom2.'</segundoApellido>'.PHP_EOL;     
            }
            else{
                $string .= '<primerApellido>'.$row[4].'</primerApellido>'.PHP_EOL;            
                $string .= '<segundoApellido/>'.PHP_EOL; 
            }
            //NISS PARTICIPANT
            $row[6] != '' ? $string .= '<niss>'.$row[6].'</niss>'.PHP_EOL :  $string .= '<niss/>'.PHP_EOL;
            //CIF EMPRESA
            $row[7] != '' ? $string .= '<cifEmpresa>'.$row[7].'</cifEmpresa>'.PHP_EOL :  $string .= '<cifEmpresa/>'.PHP_EOL;
            //Nº SS EMPRESA
            $row[8] != '' ? $string .= '<ctaCotizacion>'.$row[8].'</ctaCotizacion>'.PHP_EOL :  $string .= '<ctaCotizacion/>'.PHP_EOL;
            //FECHA NAIXEMENT
            $row[9] != '' ? $string .= '<fechaNacimiento>'.$row[9].'</fechaNacimiento>'.PHP_EOL :  $string .= '<fechaNacimiento/>'.PHP_EOL;

                //PER ROL ORGANITZADORA
            if($tipusRol == '1'){
                //EMAIL
                $row[10] != '' ? $string .= '<email>'.$row[10].'</email>'.PHP_EOL :  $string .= '<email/>'.PHP_EOL;
                //TELEFON
                $row[11] != '' ? $string .= '<telefono>'.$row[12].'</telefono>'.PHP_EOL :  $string .= '<telefono/>'.PHP_EOL;
                //SEXE
                switch ($row[11]) {
                    case '2':
                         $string .= '<sexo>F</sexo>'.PHP_EOL;//FEMENÍ
                        break;  
                    case '1':
                        $string .= '<sexo>M</sexo>'.PHP_EOL;//MASCULÍ
                        break;    
                    default:
                        $string .= '<sexo/>'.PHP_EOL;//ERROR
                        break; 
                } 
            }
            else{//PER ROL BONIFICADA
                //SEXE
                switch ($row[11]) {
                    case '2':
                         $string .= '<sexo>F</sexo>'.PHP_EOL;//FEMENÍ
                        break;  
                    case '1':
                        $string .= '<sexo>M</sexo>'.PHP_EOL;//MASCULÍ
                        break;    
                    default:
                        $string .= '<sexo/>'.PHP_EOL;//ERROR
                        break; 
                } 
                //EMAIL
                $row[10] != '' ? $string .= '<email>'.$row[10].'</email>'.PHP_EOL :  $string .= '<email/>'.PHP_EOL;
                //TELEFON
                $row[11] != '' ? $string .= '<telefono>'.$row[12].'</telefono>'.PHP_EOL :  $string .= '<telefono/>'.PHP_EOL;                
            }
            
            //DISCAPACITAT
            switch ($row[13]) {
                case 'Si':
                    $string .= '<discapacidad>true</discapacidad>'.PHP_EOL; //TIPUS DE DOCUMMENT -> DNI
                    break;
                case 'No':
                   $string .= '<discapacidad>false</discapacidad>'.PHP_EOL; //TIPUS DE DOCUMMENT -> NIE
                    break; 
                default:
                   $string .= '<discapacidad>false</discapacidad>'.PHP_EOL; //ERROR
                    break; 
            } 
            //AFECTADO TERRORISMO
            $string .= '<afectadosTerrorismo>false</afectadosTerrorismo>'.PHP_EOL;
            //VIOLENCIA GENERO
            $string .= '<afectadosViolenciaGenero>false</afectadosViolenciaGenero>'.PHP_EOL;
            //CATEGORIA PROFESIONAL
            $row[14] != '' ? $string .= '<categoriaprofesional>'.$row[14].'</categoriaprofesional>'.PHP_EOL :  $string .= '<categoriaprofesional/>'.PHP_EOL;
            //GRUP COTITZACIÓ
            $row[15] != '' ? $string .= '<grupocotizacion>'.$row[15].'</grupocotizacion>'.PHP_EOL :  $string .= '<grupocotizacion/>'.PHP_EOL;
            //NIVELL ESTUDIS
            $row[16] != '' ? $string .= '<nivelestudios>'.$row[16].'</nivelestudios>'.PHP_EOL :  $string .= '<nivelestudios/>'.PHP_EOL;
            //FECHATELEFORMACION
            //$string .= '<fechaInicioTeleformacion/>'.PHP_EOL.'<fechaFinTeleformacion/>'.PHP_EOL;
            //FIN PARTICIPANTE
            $string .= '</participante>'.PHP_EOL; 
        }
        $contador_participants ++;
    }  
    //FINALITZACIÓ DELS PARTICIPANTS
    $string .='</participantes>'.PHP_EOL;
    //AFEGIM LES DADES DELS COSTOS
    //$string .='<costes>'.PHP_EOL;

    
    $contador_costos = 1; 

    while ($row2=mysqli_fetch_array($resultCostos,MYSQLI_NUM)){
        if($contador_costos == 1){
            $string .='<costes>'.PHP_EOL;
        }
        $hihaErrorCost = false;
        $dadesErrorCost = '';
        //COMPROVACIÓ ERRORS SI ALGUN DELS CAMPS NO TE VALOR
        if($row2[1]==0){
            $dadesErrorCost .= '<p>- COSTOS DIRECTES</p>';
            $hihaErrorCost = true;
        }
        if($row2[2]==0){
            $dadesErrorCost .= '<p>- COSTOS INDIRECTES</p>';
            $hihaErrorCost = true;
        }
        if($row2[3]==0){
            $dadesErrorCost .= '<p>- COSTOS ORGANITZACIO</p>';
            $hihaErrorCost = true;
        }
        if($row2[4]==0){
            $dadesErrorCost .= '<p>- COSTOS SALARIALS</p>';
            $hihaErrorCost = true;
        }         

        if($hihaErrorCost){
            //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
                $alumn = new Alumne($row2[0], $dadesErrorCost, $contador_costos); 
                //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
                $llista_errors_costos->append($alumn);
        } 

        if(count($row2) == 5){
            //INICI DELS FORMADORS
            $string .='<coste>'.PHP_EOL;
            //cifagrupada        
            $row2[0] != '' ? $string .= '<cifagrupada>'.$row2[0].'</cifagrupada>'.PHP_EOL :  $string .= '<cifagrupada/>'.PHP_EOL; 
            //directosdirectos            
            $row2[1] != '' ? $string .= '<directos>'.$row2[1].'</directos>'.PHP_EOL :  $string .= '<directos/>'.PHP_EOL; 
            //indirectos
            $row2[2] != '' ? $string .= '<indirectos>'.$row2[2].'</indirectos>'.PHP_EOL :  $string .= '<indirectos/>'.PHP_EOL; 
            //organizacion
            $row2[3] != '' ? $string .= '<organizacion>'.$row2[3].'</organizacion>'.PHP_EOL :  $string .= '<organizacion/>'.PHP_EOL; 
            //salariales
            $row2[4] != '' ? $string .= '<salariales>'.$row2[4].'</salariales>'.PHP_EOL :  $string .= '<salariales/>'.PHP_EOL;      
            //FI DELS FORMADORS
            $string .='</coste>'.PHP_EOL;
        }
        $contador_costos ++;
    }
    //FI COSTOS
    if($contador_costos > 1){
        $string .='</costes>'.PHP_EOL;
    }
    
    $string .='</grupo>'.PHP_EOL;
    $string .='</grupos>'.PHP_EOL;
    

    $string2 = "\xFF\xFE".iconv("UTF-8","UCS-2LE",$string);
    
    if($download){  
        $handle = fopen("TRIPARTITA_FINALITZACIO_".$codigrup.".xml", "w");
        fwrite($handle, $string2);
        fclose($handle);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename("TRIPARTITA_FINALITZACIO_".$codigrup.".xml"));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize("TRIPARTITA_FINALITZACIO_".$codigrup.".xml"));
        readfile("TRIPARTITA_FINALITZACIO_".$codigrup.".xml");
        exit;  
    }
    else{
        if(count($llista_errors_participants)>0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 
            //LLISTA FORMADORS AMB ERRORS
            $llista_costos=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            //CREACIÓ DE LA LINEA QUE MOSTRA ELS PARTICIPANTS AMB ERRORS A LES DADES
            foreach($llista_errors_participants as $key=>$value) {
                if($index%2==0){
                    $llist_alumn .= '<tr class="danger"><td>'.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td colspan="2">'.$value->info.'</td></tr>';   
                }
                else $llist_alumn .= '<tr style="background-color: darkgray;" ><td>'.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td colspan="2">'.$value->info.'</td></tr>';   
                
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

             
            //VARIALBE CONTADOR PER SABER EL Nº DE FORMADOR
            $index=0;
            //CREACIÓ DE LA LINEA QUE MOSTRA ELS FORMADORS AMB ERRORS A LES DADES
            foreach($llista_errors_costos as $key=>$value) {
                $llista_costos .= '<tr class="danger"><td>EMPRESA - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td colspan="2">'.$value->info.'</td></tr>';   
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="4"><h2>LLISTA DE PARTICIPANTS AMB ERROR DE DADES</h2></th>
                    </tr>
                    <tr> 
                      <th><h4>PARTICIPANT</h4></th>
                      <th><h4>DNI</h4></th> 
                      <th colspan="2"><h4>DADES QUE FALTEN</h4></th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>';

                  if($llista_costos!=''){
                     $errors_content.='
                          <thead>
                            <tr> 
                              <th colspan="4"><h2>LLISTA D´EMPRESES AMB ERRORS DE DADES</h2></th>
                            </tr>
                          </thead>
                          <tbody>
                            '.$llista_costos.
                            '
                          </tbody>';                       
                  }                  
                  $errors_content.='</table><div class="row">
                            <div class="col-md-6">
                                <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                            </div>
                            <div class="col-md-6">
                                <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                            </div>
                        </div>
                      </div>
                    </div>';  
                 
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            $handle = fopen("TRIPARTITA_FINALITZACIO_".$codigrup.".xml", "w");
            fwrite($handle, $string2);
            fclose($handle);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename("TRIPARTITA_FINALITZACIO_".$codigrup.".xml"));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize("TRIPARTITA_FINALITZACIO_".$codigrup.".xml"));
            readfile("TRIPARTITA_FINALITZACIO_".$codigrup.".xml");
            exit;   
        }

    } 
    
}
 
/*
* Function to get all data from specific group to create a XML
* In this case, because of the imposibility to codify to UTF-16LE the xml I dicided to create the xml
* manualy, adding the content in a string and then writting it to a file
* However, I left the old code commented
*/
function getConforcatGestioParticipantsXml($download, $codigrup)
{   
    
    //MODIFICAT 08/02/2018
    //$sql ="SELECT SUBSTRING(g.`Descripcion`, 5, 2), SUBSTRING(g.`Descripcion`, 8, 2), if(mt.estado = 3, 1,0), a.snombre, a.sapellidos, a.CDNICIF, a.COBSCLI, ad.`codi collectiu`, a.idsexo, DATE_FORMAT(a.nacimiento, '%d/%m/%Y'), coalesce(ad.discapacidad,''), a.Cdomicilio, a.CPTLCLI, SUBSTRING(munCa.codi,1,5), if(a.movil = '', a.CTFO1CLI, a.movil), a.email, ad.`demandant ocupacio`, af.`CodiConforcat`,  ca.`CodiConforcat`, SUBSTRING(est.`Id`, 1, 1), gad.expedient FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.alumnos_tab_ad AS  ad ON ad.`CCODCLI` = a.`CCODCLI` LEFT JOIN softaula.municipios as mun on mun.`nombre` = a.`CPOBCLI` LEFT JOIN softaula.municipis_idscat as munCa on munCa.`nom` = mun.`nombre` LEFT JOIN softaula.estudis AS est ON est.`Valor` = ad.`estudis` LEFT JOIN softaula.`area funcional` AS af ON af.`Valor` = ad.`area funcional` LEFT JOIN softaula.`categoria` AS ca ON ca.`Valor` = ad.`categoria` LEFT JOIN softaula.grupos_tab_ad as gad on gad.codigogrupo = g.codigogrupo   WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 ORDER BY a.sapellidos";
    $sql ="SELECT SUBSTRING(g.`Descripcion`, 5, 2) AS 'N1', SUBSTRING(g.`Descripcion`, 8, 2) AS 'N2', IF(mt.estado = 3, 1,0) 'BAIXA', a.snombre AS 'NOM', a.sapellidos AS 'COGNOM', a.CDNICIF AS 'DNI', a.COBSCLI AS 'NUM SS', ad.`codi collectiu` AS 'COLECTIU', a.idsexo AS 'SEXO', DATE_FORMAT(a.nacimiento, '%d/%m/%Y') AS 'DATA NAIXEMENT', COALESCE(ad.discapacidad,'') AS 'DISCAPACITAT', a.Cdomicilio AS 'DOMICILI', a.CPTLCLI AS 'CODI POSTAL', SUBSTRING(munCa.codi,1,5) AS 'CODI MUNICIPI', IF(a.movil = '', a.CTFO1CLI, a.movil) AS 'MOVIL', a.email AS 'EMAIL', ad.`demandant ocupacio` AS 'DEMANDANT OCUPACIO', af.`CodiConforcat` AS 'AREA FUNCIONAL', ca.`CodiConforcat` AS 'CATEGORIA', SUBSTRING(est.`Id`, 1, 1) AS 'ESTUDIS', gad.expedient as 'EXPEDIENT', a.tipodoc as 'TIPO-DOC', ad.aturat_llarga_durada as 'ATURAT LL. DURADA' FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.alumnos_tab_ad AS  ad ON ad.`CCODCLI` = a.`CCODCLI` LEFT JOIN softaula.municipis_idscat AS munCa ON munCa.`nom_relacio` = a.`CPOBCLI` LEFT JOIN softaula.estudis AS est ON est.`Valor` = ad.`estudis` LEFT JOIN softaula.`area funcional` AS af ON af.`Valor` = ad.`area funcional` LEFT JOIN softaula.`categoria` AS ca ON ca.`Valor` = ad.`categoria` LEFT JOIN softaula.grupos_tab_ad AS gad ON gad.codigogrupo = g.codigogrupo WHERE g.codigogrupo='".$codigrup."' AND mt.estado != 3 ORDER BY a.sapellidos";

     //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();
     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");    
    $result = mysqli_query($db, $sql);  

    if($result->num_rows == 0){
        header("Location: index.php");
    }

    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors_conforcat = new ArrayObject();        
    //RECORREM L'ARRAY AMB TOTES LES DADES PER VEURE SI HI HA ALGUN ERROR
    $comprovacioErrors = mysqli_query($db, $sql);  
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row1=mysqli_fetch_array($comprovacioErrors,MYSQLI_ASSOC)) {
        $dadesQueFalta = '';
        $faltaDades = false;
        $i=0;
        $arrayDadesNoObligatories = array('N1', 'N2', 'BAIXA', 'EMAIL', 'EXPEDIENT', 'DISCAPACITAT', 'ATURAT LL. DURADA');
        foreach($row1 as $key=>$value) {
            //echo("key --> ".$key).'   Value-->'.$value.'</BR>';
            if($key=='SEXO' && $value<1){
                $faltaDades = true;
                $dadesQueFalta .= '<p>- '.$key.' </p>';
            }
            else{
                if($value == '' && !in_array($key, $arrayDadesNoObligatories))
                {
                    //echo $i.'- '.$key.'</br>';
                    $faltaDades = true;
                    $dadesQueFalta .= '<p>- '.$key.' </p>';
                }
            }
            $i++;
        } 
        if($faltaDades){
            //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
            $alumn = new Alumne($row1['DNI'], $dadesQueFalta, $contador_participants); 
            //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
            $llista_errors_conforcat->append($alumn);
        }
        $contador_participants ++;
    }
    
    //CREACIÓ DOCUMENT XML
    //$xml = new SimpleXMLElement('<NewDataSet />');
    $string2 = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.'<NewDataSet/>';
    $string = '<?xml version="1.0" encoding="UTF-16LE"?>'.PHP_EOL.'<NewDataSet>'.PHP_EOL;
 
    //$xml = new SimpleXMLElement($string2, null, false);

    //VARIABLE ON GUARDEM L'EXPEDIENT AL QUE PERTANY EL GRUP. EN AQUEST DOCUMENT NOMÉS SERVEIX PER DONAR NOM AL FITXER
    $expedient = '';
                         
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($result,MYSQLI_NUM)) 
    {  
        //COM NO TENIM ELS COGNOMS SEPARATS, ELS SEPAREM A PARTIR D'UN ESPAI
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]."<br/>"; 
        if(count($row) == 23)
        {    
            //ACONSEGUIM EL CODI D'EXPEDIENT DEL GRUP PER POSAR-LO AL NOM DEL FITXER
            if($contador_participants == 1){
                $row[20] != '' ? $expedient=$row[20] : $expedient='FALTA_CODI_EXPEDIENT_GRUP'; 
            }
            //ARRAY QUE GUARDA LA POSICIO DELS CAMPS NO OBLIGATORIS EN LA CONSULTA SQL PRINCIPAL PER AIXÍ COMPROVAR QUE SI FALTEN DADES SIGUIN DE LES QUE SON OBLIGATORIES
            
            
            //$participante = $xml->addChild('Participants');
            $string .= '<Participants>'.PHP_EOL;

            $row[0] != '' ? $string .= '<NumAccio>'.$row[0].'</NumAccio>'.PHP_EOL :  $string .= '<NumAccio/>'.PHP_EOL;
            //$participante->addChild('NumAccio', $row[0]); //NUMERO ACCIÓ   

            $row[1] != '' ? $string .= '<NumGrup>'.$row[1].'</NumGrup>'.PHP_EOL : $string .= '<NumGrup/>'.PHP_EOL;
            //$participante->addChild('NumGrup', $row[1]); //NUMERO GRUP     

            $row[2] != '' ? $string .= '<Baixa>'.$row[2].'</Baixa>'.PHP_EOL : $string .= '<Baixa/>'.PHP_EOL;
            //$participante->addChild('Baixa', $row[2]); //NUMERO GRUP  

            $row[3] != '' ? $string .= '<Nom>'.$row[3].'</Nom>'.PHP_EOL : $string .= '<Nom/>'.PHP_EOL;
            //$participante->addChild('Nom', $row[3]); //Nom   

            $row[4] != '' ? $string .= '<Cognoms>'.$row[4].'</Cognoms>'.PHP_EOL : $string .= '<Cognoms/>'.PHP_EOL;
            //$participante->addChild('Cognoms', $row[4]); //Cognoms    

            $row[5] != '' ? $string .= '<NIF>'.$row[5].'</NIF>'.PHP_EOL : $string .= '<NIF/>'.PHP_EOL;
            //$participante->addChild('NIF', $row[5]); //NIF   

            //TIPO DOCUMENTO
            switch ($row[21]) {                
                case '0':
                    $string .= '<TipusDocument>1</TipusDocument>'.PHP_EOL;
                    //$participante->addChild('SexeH', '1'); //HOME
                    break;
                case '1':
                    $string .= '<TipusDocument>2</TipusDocument>'.PHP_EOL;
                    //$participante->addChild('SexeH', '0'); //DONA
                    break;
                default:
                    $string .= '<TipusDocument/>'.PHP_EOL;
                    //$participante->addChild('SexeH'); //ERROR DADA                     
                    break; 
            }

            $row[6] != '' ? $string .= '<NASS>'.$row[6].'</NASS>'.PHP_EOL : $string .= '<NASS/>'.PHP_EOL;
            //$participante->addChild('NASS', $row[6]); //NASS   

            $row[7] != '' ? $string .= '<Colectiu>'.$row[7].'</Colectiu>'.PHP_EOL : $string .= '<Colectiu/>'.PHP_EOL;
            //$participante->addChild('Colectiu', $row[7]); //Colectiu   

            //SEXE
            switch ($row[8]) {                
                case '1':
                    $string .= '<SexeH>1</SexeH>'.PHP_EOL;
                    //$participante->addChild('SexeH', '1'); //HOME
                    break;
                case '2':
                    $string .= '<SexeH>0</SexeH>'.PHP_EOL;
                    //$participante->addChild('SexeH', '0'); //DONA
                    break;
                default:
                    $string .= '<SexeH/>'.PHP_EOL;
                    //$participante->addChild('SexeH'); //ERROR DADA                     
                    break; 
            }

            $row[9] != '' ? $string .= '<dataNaixement>'.$row[9].'</dataNaixement>'.PHP_EOL : $string .= '<dataNaixement/>'.PHP_EOL;            
            //$participante->addChild('dataNaixement', $row[9]); //dataNaixement

            //DISCAPACITAT
            switch ($row[10]) {
                case 'Si':
                    $string .= '<Discapacitat>1</Discapacitat>'.PHP_EOL;
                    //$participante->addChild('Discapacitat', '1'); //SI
                    break;
                case 'No':
                    $string .= '<Discapacitat>0</Discapacitat>'.PHP_EOL;
                    //$participante->addChild('Discapacitat', '0'); //NO
                    break; 
                default:
                    $string .= '<Discapacitat>0</Discapacitat>'.PHP_EOL;
                    //$participante->addChild('Discapacitat'); //ERROR DADA
                    break;
            }

            $row[11] != '' ? $string .= '<Adreca>'.$row[11].'</Adreca>'.PHP_EOL : $string .= '<Adreca/>'.PHP_EOL; 
            //$participante->addChild('Adreca', $row[11]); //Adreca

            $row[12] != '' ? $string .= '<CP>'.$row[12].'</CP>'.PHP_EOL : $string .= '<CP/>'.PHP_EOL; 
            //$participante->addChild('CP', $row[12]); //CP

            $row[13] != '' ? $string .= '<CodiMunicipi>'.$row[13].'</CodiMunicipi>'.PHP_EOL : $string .= '<CodiMunicipi/>'.PHP_EOL; 
            //$participante->addChild('CodiMunicipi', $row[13]); //CodiMunicipi

            if($row[14] != ''){
                $string .= '<Telefon1>'.$row[14].'</Telefon1>'.PHP_EOL;
            }
            else $string .= '<Telefon1/>'.PHP_EOL; 
            //$participante->addChild('Telefon1', $row[14]); //Telefon1

            $string .= '<Telefon2/>'.PHP_EOL; 
            //$participante->addChild('Telefon2'); //Telefon2

            $row[15] != '' ? $string .= '<Email>'.$row[15].'</Email>'.PHP_EOL : $string .= '<Email/>'.PHP_EOL; 
            //$participante->addChild('Email', $row[15]); //Email
            
            //DemandantOcupacio
            switch ($row[16]) {
                case 'Si':
                    $string .= '<DemandantOcupacio>1</DemandantOcupacio>'.PHP_EOL;
                    //$participante->addChild('DemandantOcupacio', '1'); //Si
                    break;
                case 'No':
                    $string .= '<DemandantOcupacio>0</DemandantOcupacio>'.PHP_EOL;
                    //$participante->addChild('DemandantOcupacio', '0'); //No
                    break; 
                default:
                    $string .= '<DemandantOcupacio/>'.PHP_EOL;
                    //$participante->addChild('DemandantOcupacio'); //ERROR DADES
                    break;
            }

            $row[17] != '' ? $string .= '<Area>'.$row[17].'</Area>'.PHP_EOL : $string .= '<Area/>'.PHP_EOL; 
            //$participante->addChild('Area', $row[17]); //Area

            $row[18] != '' ? $string .= '<Categories>'.$row[18].'</Categories>'.PHP_EOL : $string .= '<Categories/>'.PHP_EOL; 
            //$participante->addChild('Categories', $row[18]); //Categories

            $row[19] != '' ? $string .= '<Estudis>'.$row[19].'</Estudis>'.PHP_EOL : $string .= '<Estudis/>'.PHP_EOL; 
            //$participante->addChild('Estudis', $row[19]); //Estudis 

            //ATURAT LLARGA DURADA
            switch ($row[22]) {                
                case 'Si':
                    $string .= '<AturatLLargaDurada>1</AturatLLargaDurada>'.PHP_EOL;
                    //$participante->addChild('SexeH', '1'); //HOME
                    break; 
                default:
                    $string .= '<AturatLLargaDurada>0</AturatLLargaDurada>'.PHP_EOL;
                    //$participante->addChild('SexeH'); //ERROR DADA                     
                    break; 
            }

            $string.='</Participants>'.PHP_EOL;

        }

        $contador_participants ++;
    } 
    $string .= '</NewDataSet>'; 
    $string = "\xFF\xFE".iconv("UTF-8","UCS-2LE",$string);
    
    if($download){         
        $handle = fopen("CFCP_".$expedient.".xml", "w");
        fwrite($handle, $string);
        fclose($handle);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename("CFCP_".$expedient.".xml"));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize("CFCP_".$expedient.".xml"));
        readfile("CFCP_".$expedient.".xml");
        exit;
        //header('Content-type: text/xml; charset="UTF-16"');
        //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
        //print($xml->asXML());
        
    }
    else{
        if(count($llista_errors_conforcat)>0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            foreach($llista_errors_conforcat as $key=>$value) {
                if($index%2==0){
                    $llist_alumn .= '<tr class="danger"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';   
                }
                else{
                    $llist_alumn .= '<tr style="background-color: darkgray;"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';   
                }  
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="3"><h2>LLISTA DE PARTICIPANTS AMB ERROR DE DADES</h2></th>
                    </tr>
                    <tr> 
                      <th colspan="3"><h3>* Camps que <b style="color:red;">poden</b> estar buits:</h3> <h4>1. Baixa</h4> <h4>2. Telefon2</h4> <h4>3. Email</h4></th>
                    </tr>
                  </thead>
                  <thead>
                    <tr> 
                      <th>Nº PARTICIPANT</th>
                      <th>DNI ALUMNE</th>
                      <th>NOM</th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                    </div>
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                    </div>
                </div>
              </div>
            </div>';   
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            $handle = fopen("CFCP_".$expedient.".xml", "w");
            fwrite($handle, $string);
            fclose($handle);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename("CFCP_".$expedient.".xml"));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize("CFCP_".$expedient.".xml"));
            readfile("CFCP_".$expedient.".xml");
            exit;
            //header('Content-type: text/xml; charset="UTF-16"');
            //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
            //print($xml->asXML());
        }

    }

    //free result set 
    mysqli_free_result($result); 
     //close connection 
    mysqli_close($db); 
    
    
}

/*
* Function to get all data from specific group to create a XML
* In this case, because of the imposibility to codify to UTF-16LE the xml I dicided to create the xml
* manualy, adding the content in a string and then writting it to a file
* However, I left the old code commented
*/
function getConforcatCertificacioParticipantsXml($download, $codigrup)
{   
     //OBTENCIÓ DE LES DADES DE L´ALUMNE 
    $sqlGeneral ="SELECT DISTINCT SUBSTRING(g.`Descripcion`, 5, 2) AS 'N1', SUBSTRING(g.`Descripcion`, 8, 2) AS 'N2', a.CDNICIF AS 'DNI', a.snombre AS 'NOM', a.sapellidos AS 'COGNOM', IF(mt.estado = 3, 'A','F') AS 'ESTAT', cl.cnomcli AS 'NOM EMPRESA',cl.cdnicif AS 'CIF EMPRESA', cl.cptlcli AS 'CODI POSTAL EMPRESA', SUBSTRING(munCa.codi,1,5) 'CODI MUNICIPI', cl.cdomicilio AS 'DIRECCIO EMPRESA', LEFT(clad.pyme,1) AS 'ES PYME', clad.sector AS 'SECTOR EMPRESA', cl.cobscli AS 'NUM SS EMPRESA', cl.CTFo1cli AS 'TELEFON EMPRESA', gad.expedient AS 'EXPEDIENT' FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.`clientes` AS cl ON cl.ccodcli =  mt.`FacturarA` LEFT JOIN softaula.`clientes_tab_ad` AS clad ON clad.ccodcli =  cl.ccodcli LEFT JOIN softaula.codigos_postales AS cp ON cp.`poblacion` = cl.`CPOBCLI` LEFT JOIN softaula.municipis_idscat AS munCa ON munCa.`nom` = cp.`poblacion` LEFT JOIN softaula.grupos_tab_ad AS gad ON gad.codigogrupo = g.codigogrupo WHERE g.codigogrupo='".$codigrup."' AND mt.estado != 3 ORDER BY a.sapellidos";

     //OBTENCIÓ DE LES DADES DELS FORMADORS DELS GRUPS
    $sqlFormadors ="SELECT DISTINCT p.`sNombre` AS 'NOM FORMADOR', p.`sApellidos` AS 'COGNOM FORMADOR', p.`DNI` AS 'DNI FORMADOR', p.`TFO1` AS 'TELEFON FORMADOR' FROM softaula.`gruposl` AS gl LEFT JOIN softaula.`profesor` AS p ON p.`indice` = gl.`IdProfesor` WHERE gl.`CodigoGrupo` = '".$codigrup."' AND p.`indice` >0";

     //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();

    $expedient ='FALTA_CODI_EXPEDIENT';

     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");    
    $resultGeneral = mysqli_query($db, $sqlGeneral);  
    $resultFormadors = mysqli_query($db, $sqlFormadors);  

    if($resultGeneral->num_rows == 0){
        header("Location: index.php");
    }
/*
    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors_participants = new ArrayObject();   
    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE FORMADOR
    $llista_errors_formadors = new ArrayObject();   
    //String on guardar els errors del grup
    $errors_grup = '' ;
    //LLISTA D'ERRORS DELS FORMADORS
*/

    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors_participants = new ArrayObject();  
    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE FORMADOR
    $llista_errors_formadors = new ArrayObject();         
    //RECORREM L'ARRAY AMB TOTES LES DADES PER VEURE SI HI HA ALGUN ERROR
    $comprovacioErrors = mysqli_query($db, $sqlGeneral);  
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row1=mysqli_fetch_array($comprovacioErrors,MYSQLI_ASSOC)) {
        $dadesQueFalta = '';
        $faltaDades = false;
        $i=0;
        $arrayDadesNoObligatories = array('N1', 'N2', 'EXPEDIENT');
        foreach($row1 as $key=>$value) {
            //echo("key --> ".$key).'   Value-->'.$value.'</BR>';
            if($value == '' && !in_array($key, $arrayDadesNoObligatories)){
                //echo $i.'- '.$key.'</br>';
                $faltaDades = true;
                $dadesQueFalta .= '<p>- '.$key.' </p>';
            }            
                $i++;
        } 
        if($faltaDades){
            //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
            $alumn = new Alumne($row1['DNI'], $dadesQueFalta, $contador_participants); 
            //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
            $llista_errors_participants->append($alumn);
        }
        $contador_participants ++;
        //echo("</BR>");
    }
    
 
                         
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($resultGeneral,MYSQLI_NUM)) 
    {   
        /* 
        //ARRAY QUE GUARDA LA POSICIO DELS CAMPS NO OBLIGATORIS EN LA CONSULTA SQL PRINCIPAL PER AIXÍ COMPROVAR QUE SI FALTEN DADES SIGUIN DE LES QUE SON OBLIGATORIES
        $arrayDadesObligatories = array(12,13,14);
        //COMPROVACIÓ ERRORS SI ALGUN DELS CAMPS NO TE VALOR
        for ($index = 0; $index < count($row); $index++) {
            if (!in_array($index, $arrayDadesObligatories))//SOL COMPROVEM ELS CAMPS 
            {
                 if($row[$index] == '' || $row[$index] == null)//Si algun dels camps de
                 {
                    //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
                    $alumn = new Alumne($row[2], $row[3]." ".$row[4], $contador_participants); 
                    //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
                    $llista_errors_participants->append($alumn);
                    break;
                 }
            }
            
        }*/   
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]." -- ".$row[7]." -- ".$row[8]." -- ".$row[9]." -- ".$row[10]." -- ".$row[11]." -- ".$row[12]." -- ".$row[13]." -- ".$row[14]."<br/>"; 
        if(count($row) == 16)
        {   
            //Quan és el primer participant creem les linees que només apareixen un cop
            if($contador_participants == 1)
            {
                //CREACIÓ DOCUMENT XML A DINS D'UNA STRING PER DESPRÉS IMPRIMIR-LA    
                $string = '<?xml version="1.0" encoding="UTF-16LE"?>'.PHP_EOL.'<MODEL_DS15>'.PHP_EOL;
                //EXPEDIENT
                if($row[15] != ''){
                    $string .= '<Expedient>'.$row[15].'</Expedient>'.PHP_EOL;
                    $expedient = $row[15];
                }else{
                    $string .= '<Expedient/>'.PHP_EOL;
                    $expedient = 'FALTA_CODI_EXPEDIENT';
                }
                $string .='<CifEntitatExecutant>B25026428</CifEntitatExecutant>'.PHP_EOL.'<GrupAccioFormativa>'.PHP_EOL;

                //Numero Acció Formativa
                $row[0] != '' ? $string .= '<NumeroAccioFormativa>'.$row[0].'</NumeroAccioFormativa>'.PHP_EOL :  $string .= '<NumeroAccioFormativa/>'.PHP_EOL; 
                //Numero Grup
                $row[1] != '' ? $string .= '<NumeroGrup>'.$row[1].'</NumeroGrup>'.PHP_EOL :  $string .= '<NumeroGrup/>'.PHP_EOL; 
                //Grup a Certificar 
                $string .= '<Grup_a_Certificar>S</Grup_a_Certificar>'.PHP_EOL; 
                //Grup Objecte de Control
                $string .= '<Grup_Objecte_de_Control>N</Grup_Objecte_de_Control>'.PHP_EOL; 
                //Generar DS15
                $string .= '<Generar_DS15>N</Generar_DS15>'.PHP_EOL; 
                //Informacio adicional
                $string .= '<Informacio_Addicional_Memoria>N</Informacio_Addicional_Memoria>'.PHP_EOL; 
            } 
                //INICI PARTICIPANTS
            $string .= '<Participants>'.PHP_EOL;
            //NIF PARTICIPANT
            $row[2] != '' ? $string .= '<NifParticipant>'.$row[2].'</NifParticipant>'.PHP_EOL :  $string .= '<NifParticipant/>'.PHP_EOL; 
            //NOM PARTICIPANT
            $row[3] != '' ? $string .= '<NomParticipant>'.$row[3].'</NomParticipant>'.PHP_EOL :  $string .= '<NomParticipant/>'.PHP_EOL;
            //COGNOM PARTICIPANT
            $row[4] != '' ? $string .= '<CognomsParticipant>'.$row[4].'</CognomsParticipant>'.PHP_EOL :  $string .= '<CognomsParticipant/>'.PHP_EOL;
            //ESTAT PARTICIPANT
            $row[5] != '' ? $string .= '<EstatParticipant>'.$row[5].'</EstatParticipant>'.PHP_EOL :  $string .= '<EstatParticipant/>'.PHP_EOL;
            //PARTICIPANT A CERTIFICAR
            $string .= '<Participant_a_Certificar>N</Participant_a_Certificar>'.PHP_EOL; 
            //Reciclatge Requalificacio
            $string .= '<ReciclatgeRequalificacio>N</ReciclatgeRequalificacio>'.PHP_EOL;

                // DADES DE L'EMPRESA DE CADA PARTICIPANT
                if($row[7]!='') // PER DIFERENCIAR ENTRE OCUPAT I DESOCUPAT
                {
                    $string .= '<Ocupats>'.PHP_EOL;
                    //RAO SOCIAL
                    $row[6] != '' ? $string .= '<RaoSocial>'.$row[6].'</RaoSocial>'.PHP_EOL :  $string .= '<RaoSocial/>'.PHP_EOL;
                    //CifEmpresa
                    $row[7] != '' ? $string .= '<CifEmpresa>'.$row[7].'</CifEmpresa>'.PHP_EOL :  $string .= '<CifEmpresa/>'.PHP_EOL;
                    //CodiPostalCentreTreball
                    $row[8] != '' ? $string .= '<CodiPostalCentreTreball>'.$row[8].'</CodiPostalCentreTreball>'.PHP_EOL :  $string .= '<CodiPostalCentreTreball/>'.PHP_EOL;
                    //IdMunicipiCentreTreball
                    $row[9] != '' ? $string .= '<IdMunicipiCentreTreball>'.$row[9].'</IdMunicipiCentreTreball>'.PHP_EOL :  $string .= '<IdMunicipiCentreTreball/>'.PHP_EOL;
                    //AdreçaCentreTreball
                    $row[10] != '' ? $string .= '<AdreçaCentreTreball>'.$row[10].'</AdreçaCentreTreball>'.PHP_EOL :  $string .= '<AdreçaCentreTreball/>'.PHP_EOL;
                    //EsPYME
                    $row[11] != '' ? $string .= '<EsPYME>'.$row[11].'</EsPYME>'.PHP_EOL :  $string .= '<EsPYME/>'.PHP_EOL;
                    //Sector
                    $row[12] != '' ? $string .= '<Sector>'.$row[12].'</Sector>'.PHP_EOL :  $string .= '<Sector/>'.PHP_EOL;
                    //ReferenciaConveni
                    $string .= '<ReferenciaConveni/>'.PHP_EOL;
                    //NumInscripcioSS
                    $row[13] != '' ? $string .= '<NumInscripcioSS>'.$row[13].'</NumInscripcioSS>'.PHP_EOL :  $string .= '<NumInscripcioSS/>'.PHP_EOL;
                    //NombreTreballadors
                    $string .= '<NombreTreballadors>0</NombreTreballadors>'.PHP_EOL;
                    //Telefon1
                    $row[14] != '' ? $string .= '<Telefon1>'.$row[14].'</Telefon1>'.PHP_EOL :  $string .= '<Telefon1/>'.PHP_EOL;
                    //Email
                    $string .= '<Email/>'.PHP_EOL;
                    //FINALITZACIÓ DADES EMPRESA
                    $string .= '</Ocupats>'.PHP_EOL;
                } 
            //finalització d'un participant
            $string .= '</Participants>'.PHP_EOL;
        }
        $contador_participants ++;
    } 

    //AFEGIM LES DADES DELS FOMADORS

    $contador_formadors = 1; 
    $comprovacioErrorsFormadors = mysqli_query($db, $sqlFormadors); 
    while ($row3=mysqli_fetch_array($comprovacioErrorsFormadors,MYSQLI_ASSOC)) {
        $dadesQueFalta = '';
        $faltaDades = false;
        $i=0;
        $arrayDadesNoObligatories = array();
        foreach($row3 as $key=>$value) {
            //echo("key --> ".$key).'   Value-->'.$value.'</BR>';
            if($value == '' && !in_array($key, $arrayDadesNoObligatories)){
                //echo $i.'- '.$key.'</br>';
                $faltaDades = true;
                $dadesQueFalta .= '<p>- '.$key.' </p>';
            }            
                $i++;
        } 
        if($faltaDades){
            //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
            $alumn = new Alumne($row3['DNI'], $dadesQueFalta, $contador_formadors); 
            //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
            $llista_errors_formadors->append($alumn);
        }
        $contador_participants ++;
        //echo("</BR>");
    }
    
    while ($row2=mysqli_fetch_array($resultFormadors,MYSQLI_NUM)){
                
        if(count($row2) == 4){
            //INICI DELS FORMADORS
            $string .='<Formadors>'.PHP_EOL;
            //NOM FORMADOR            
            $row2[0] != '' ? $string .= '<NomFormador>'.$row2[0].'</NomFormador>'.PHP_EOL :  $string .= '<NomFormador/>'.PHP_EOL; 
            //CognomsFormador            
            $row2[1] != '' ? $string .= '<CognomsFormador>'.$row2[1].'</CognomsFormador>'.PHP_EOL :  $string .= '<CognomsFormador/>'.PHP_EOL; 
            //NifFormador
            $row2[2] != '' ? $string .= '<NifFormador>'.$row2[2].'</NifFormador>'.PHP_EOL :  $string .= '<NifFormador/>'.PHP_EOL; 
            //Telefon1Formador
            $row2[3] != '' ? $string .= '<Telefon1Formador>'.$row2[3].'</Telefon1Formador>'.PHP_EOL :  $string .= '<Telefon1Formador/>'.PHP_EOL; 
            //Telefon2Formador
            $row2[3] != '' ? $string .= '<Telefon2Formador>'.$row2[3].'</Telefon2Formador>'.PHP_EOL :  $string .= '<Telefon2Formador/>'.PHP_EOL;      
            //FI DELS FORMADORS
            $string .='</Formadors>'.PHP_EOL;
        }
        $contador_formadors ++;
    }
    //FI GRUP ACCIÓ FORMATIVA I FINALITZACIÓ DOCUMENT
    $string .='</GrupAccioFormativa>'.PHP_EOL.'</MODEL_DS15>'; 

    $string = "\xFF\xFE".iconv("UTF-8","UCS-2LE",$string);
    
    if($download){  
        $handle = fopen("CFCDS15_".$expedient.".xml", "w");
        fwrite($handle, $string);
        fclose($handle);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename("CFCDS15_".$expedient.".xml"));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize("CFCDS15_".$expedient.".xml"));
        readfile("CFCDS15_".$expedient.".xml");
        exit; 
        //header('Content-type: text/xml; charset="UTF-16"');
        //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
        //print($xml->asXML());
        
    }
    else{
        if(count($llista_errors_participants)>0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 
            //LLISTA FORMADORS AMB ERRORS
            $llista_formadors=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            //CREACIÓ DE LA LINEA QUE MOSTRA ELS PARTICIPANTS AMB ERRORS A LES DADES
            foreach($llista_errors_participants as $key=>$value) {
                if($index%2==0){
                    $llist_alumn .= '<tr class="danger"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td colspan="2">'.$value->info.'</td></tr>'; 
                }
                else{
                    $llist_alumn .= '<tr style="background-color: darkgray;"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td colspan="2">'.$value->info.'</td></tr>';  
                } 
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }
             
            //VARIALBE CONTADOR PER SABER EL Nº DE FORMADOR
            $index=0;
            //CREACIÓ DE LA LINEA QUE MOSTRA ELS FORMADORS AMB ERRORS A LES DADES
            foreach($llista_errors_formadors as $key=>$value) {
                $llista_formadors .= '<tr class="danger"><td>Formador - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td colspan="2">'.$value->info.'</td></tr>';   
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="4"><h3><b style="color:red;">* Camps NO OBLIGATORIS</b></h3></th> 
                    </tr>
                    <tr>
                       <th style="vertical-align: middle;"><h4>Dades Empresa d´un participant</h4></th>
                       <th colspan="3"><h4>1. Sector</h4> <h4>2. ReferenciaConveni</h4> <h4>3. NumInscripcioSS</h4> <h4>4. NombreTreballadors</h4> <h4>5. Telefon1</h4> <h4>6. Email</h4></th>
                    </tr>
                    <tr>
                       <th><h4>Dades Formadors</h4></th>
                       <th colspan="3"><h4>1. Telefon2</h4</th>
                    </tr>
                    <tr> 
                      <th colspan="4"><h2>LLISTA DE PARTICIPANTS AMB ERROR DE DADES</h2></th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>';

                  if($llista_formadors!=''){
                     $errors_content.='
                          <thead>
                            <tr> 
                              <th colspan="4"><h2>LLISTA FORMADORS AMB FALTA DE DADES</h2></th>
                            </tr>
                          </thead>
                          <tbody>
                            '.$llista_formadors.
                            '
                          </tbody>';                       
                  }                  
                  $errors_content.='</table><div class="row">
                            <div class="col-md-6">
                                <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                            </div>
                            <div class="col-md-6">
                                <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                            </div>
                        </div>
                      </div>
                    </div>';  
                 
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            $handle = fopen("CFCDS15_".$expedient.".xml", "w");
            fwrite($handle, $string);
            fclose($handle);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename("CFCDS15_".$expedient.".xml"));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize("CFCDS15_".$expedient.".xml"));
            readfile("CFCDS15_".$expedient.".xml");
            exit;
            //header('Content-type: text/xml; charset="UTF-16"');
            //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
            //print($xml->asXML());
            
        }

    }
    /*
    //free result set 
    mysqli_free_result($result); 
     //close connection 
    mysqli_close($db); */

}



/*
* ADDED BY ALBERT
* Function to create a connexion with SOFTAULA DATA BASE
*/
function getSoftAulaBD()
{      
    //$servername = "docs.ilerna.com:3306";//PER QUAN TREBALLEM EN LOCAL
    $servername = "docs.ilerna.com";//PER QUAN TREBALLEM EN servidor
    $username = "prestashop";
    $password = "prestashop314ilerna";
    $dbname = "softaula";
    //return new mysqli($servername, $username, $password, $dbname);
    return mysqli_connect($servername, "prestashop", "prestashop314ilerna", "softaula");
}
?>
