<?php
include "error_content.php";
//CLASSE ALUMNE
class Alumne{
    public $nparticipant;
    public $ccodcli;
    public $info;

    function __construct($codi, $nom, $num){
        $this->ccodcli = $codi;
        $this->info = $nom;
        $this->nparticipant = $num;
    }
} 

//INICI PROGRAMA
if(!isset($_POST["grup"]) && empty($_POST["grup"]) && !isset($_POST["tipodoc"]) && empty($_POST["tipodoc"]) && !isset($_GET["download"])){
    header("Location: index.html");
}
else{
    //SI L'USUARI VOL FORÇAR LA DESCARREGA DEL FITXER
    if(isset($_GET["download"]) && !empty($_GET["download"]) && isset($_GET["codi"]) && !empty($_GET["codi"]) && isset($_GET["doc"]) && !empty($_GET["doc"])){
        $codigrup = $_GET["codi"];
        $tipodoc = $_GET["doc"];
        $download = 1;
    }
    else{
        $codigrup = $_POST["grup"];
        $tipodoc = $_POST["tipodoc"];
        $download = 0;
    } 

    switch ($tipodoc) {
        case '1':
            getTripartitaGestioParticipantsXml($download, $codigrup);
            break; 

        case '2':
            getConforcatGestioParticipantsXml($download, $codigrup);
            break; 

        case '3':
            getConforcatCertificacioParticipantsXml($download, $codigrup);
            break; 
    } 
    
}



/*
* Function to get all data from specific group to create the TRIPARTITA XML
*/
function getTripartitaGestioParticipantsXml($download, $codigrup)
{   
    //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();
     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");
    $sql ="SELECT distinct a.CDNICIF, a.tipodoc, a.sapellidos, a.snombre, a.COBSCLI, a.idsexo, DATE_FORMAT(a.nacimiento, '%d/%m/%Y') FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 order by a.sapellidos";
    $result = mysqli_query($db, $sql);

    if($result->num_rows == 0){
        header("Location: index.php");
    }


    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors = new ArrayObject();    
    //CREACIÓ DOCUMENT XML
    $xml = new SimpleXMLElement('<participantes xmlns="http://www.fundaciontripartita.es/schemas"/>');
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($result,MYSQLI_NUM)) 
    {        
        //COM NO TENIM ELS COGNOMS SEPARATS, ELS SEPAREM A PARTIR D'UN ESPAI
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]."<br/>"; 
        if( count($row) == 7)
        {      
            //COMPROVACIÓ ERRORS SI ALGUN DELS CAMPS NO TE VALOR
            if($row[0] =="" || $row[1]=="" || $row[3]=="" || $row[4]=="" || $row[5]=="0" || $row[6]==""){
                //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
                $alumn = new Alumne($row[0], $row[3]." ".$row[2], $contador_participants); 
                //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
                $llista_errors->append($alumn);   
            } 
            $participante = $xml->addChild('participante');
            $participante->addChild('D_DOCUMENTO', $row[0]); //NUMERO DNI O PASSAPORT        
            switch ($row[1]) {
                case '0':
                    $participante->addChild('N_TIPO_DOCUMENTO', '10'); //TIPUS DE DOCUMMENT -> DNI
                    break;
                case '1':
                   $participante->addChild('N_TIPO_DOCUMENTO', '60'); //TIPUS DE DOCUMMENT -> NIE
                    break; 
            } 
            if (strpos($row[2], ' ')) {
                $cognoms = explode(" ", $row[2]);                
                $cognom2 = "";
                for ($x = 1; $x < count($cognoms); $x++) {
                    $cognom2.=$cognoms[$x];
                }                  
                $participante->addChild('D_APELLIDO1', $cognoms[0]); //COGNOMS
                $participante->addChild('D_APELLIDO2', $cognom2); //COGNOMS                 
            }
            else{
                $participante->addChild('D_APELLIDO1', $row[2]); //COGNOMS
                $participante->addChild('D_APELLIDO2', ''); //COGNOMS
            }
            $participante->addChild('D_NOMBRE', $row[3]); //NOM
            $participante->addChild('D_NISS', $row[4]); //NUMERO SEGURETAT SOCIAL

            //SEXE
            switch ($row[5]) {
                case '2':
                    $participante->addChild('B_SEXO', 'F');//SEXE
                    break;  
                case '1':
                    $participante->addChild('B_SEXO', 'M');//SEXE
                    break;    
                case '0':
                    $participante->addChild('SexeH'); //ERROR DADA
                    break;
                
                
            } 
            $participante->addChild('F_NACIMIENTO', $row[6]); //DATA NAIXEMENT            


        }
        $contador_participants ++;
    }
    
    if($download){         
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="tripartita.xml"'); 
        print($xml->asXML());
    }
    else{
        if(count($llista_errors)>0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            foreach($llista_errors as $key=>$value) {
                $llist_alumn .= '<tr class="danger"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';   
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="3">LLISTA DE PARTICIPANTS AMB ERROR DE DADES</th>
                    </tr>
                  </thead>
                  <thead>
                    <tr> 
                      <th>Nº PARTICIPANT</th>
                      <th>DNI ALUMNE</th>
                      <th>NOM</th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                    </div>
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                    </div>
                </div>
              </div>
            </div>';   
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            header('Content-type: text/xml');
            header('Content-Disposition: attachment; filename="tripartita.xml"'); 
            print($xml->asXML());
        }

    }

    //free result set 
    mysqli_free_result($result); 
     //close connection 
    mysqli_close($db); 
    
} 

/*
* Function to get all data from specific group to create a XML
* In this case, because of the imposibility to codify to UTF-16LE the xml I dicided to create the xml
* manualy, adding the content in a string and then writting it to a file
* However, I left the old code commented
*/
function getConforcatGestioParticipantsXml($download, $codigrup)
{   
    $sql ="SELECT SUBSTRING(g.`Descripcion`, 5, 2), SUBSTRING(g.`Descripcion`, 8, 2), if(mt.estado = 3, 1,0), a.snombre, a.sapellidos, a.CDNICIF, a.COBSCLI, ad.`codi collectiu`, a.idsexo, DATE_FORMAT(a.nacimiento, '%d/%m/%Y'), coalesce(ad.discapacidad,''), a.Cdomicilio, a.CPTLCLI, SUBSTRING(munCa.codi,1,5), if(a.movil = '', a.CTFO1CLI, a.movil), a.email, ad.`demandant ocupacio`, af.`CodiConforcat`,  ca.`CodiConforcat`, SUBSTRING(est.`Id`, 1, 1) FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.alumnos_tab_ad AS  ad ON ad.`CCODCLI` = a.`CCODCLI` LEFT JOIN softaula.municipios as mun on mun.`nombre` = a.`CPOBCLI` LEFT JOIN softaula.municipis_idscat as munCa on munCa.`nom` = mun.`nombre` LEFT JOIN softaula.estudis AS est ON est.`Valor` = ad.`estudis` LEFT JOIN softaula.`area funcional` AS af ON af.`Valor` = ad.`area funcional` LEFT JOIN softaula.`categoria` AS ca ON ca.`Valor` = ad.`categoria` WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 ORDER BY a.sapellidos";
     //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();
     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");    
    $result = mysqli_query($db, $sql);  

    if($result->num_rows == 0){
        header("Location: index.php");
    }

    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors_conforcat = new ArrayObject();    
    //CREACIÓ DOCUMENT XML
    //$xml = new SimpleXMLElement('<NewDataSet />');
    $string2 = '<?xml version="1.0" encoding="UTF-8"?><NewDataSet/>';
    $string = '<?xml version="1.0" encoding="UTF-16LE"?><NewDataSet>';
 
    //$xml = new SimpleXMLElement($string2, null, false);

    
                         
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($result,MYSQLI_NUM)) 
    {        
        //COM NO TENIM ELS COGNOMS SEPARATS, ELS SEPAREM A PARTIR D'UN ESPAI
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]."<br/>"; 
        if(count($row) == 20)
        {      
            //COMPROVACIÓ ERRORS SI ALGUN DELS CAMPS NO TE VALOR
            for ($index = 0; $index < count($row); $index++) {
                if($row[$index] == '' || $row[$index] == null){
                    //CREEM UN OBJECTE ALUMNE AMB EL DNI I EL NOM I COGNOM CONCATENAT
                    $alumn = new Alumne($row[5], $row[3]." ".$row[4], $contador_participants); 
                    //AFEGIM AQUEST OBJECTE ALUMNE A LA LLISTA D'ERRORS
                    $llista_errors_conforcat->append($alumn);
                    break;
                }
            }   
            $string .= '<Participants>';
            //$participante = $xml->addChild('Participants');


            $row[0] != '' ? $string .= '<NumAccio>'.$row[0].'</NumAccio>' :  $string .= '!<NumAccio/>';
            //$participante->addChild('NumAccio', $row[0]); //NUMERO ACCIÓ   

            $row[1] != '' ? $string .= '<NumGrup>'.$row[1].'</NumGrup>' : $string .= '<NumGrup/>';
            //$participante->addChild('NumGrup', $row[1]); //NUMERO GRUP     

            $row[2] != '' ? $string .= '<Baixa>'.$row[2].'</Baixa>' : $string .= '<Baixa/>';
            //$participante->addChild('Baixa', $row[2]); //NUMERO GRUP  

            $row[3] != '' ? $string .= '<Nom>'.$row[3].'</Nom>' : $string .= '<Nom/>';
            //$participante->addChild('Nom', $row[3]); //Nom   

            $row[4] != '' ? $string .= '<Cognoms>'.$row[4].'</Cognoms>' : $string .= '<Cognoms/>';
            //$participante->addChild('Cognoms', $row[4]); //Cognoms    

            $row[5] != '' ? $string .= '<NIF>'.$row[5].'</NIF>' : $string .= '<NIF/>';
            //$participante->addChild('NIF', $row[5]); //NIF   

            $row[6] != '' ? $string .= '<NASS>'.$row[6].'</NASS>' : $string .= '<NASS/>';
            //$participante->addChild('NASS', $row[6]); //NASS   

            $row[7] != '' ? $string .= '<Colectiu>'.$row[7].'</Colectiu>' : $string .= '<Colectiu/>';
            //$participante->addChild('Colectiu', $row[7]); //Colectiu   

            //SEXE
            switch ($row[8]) {
                case '0':
                    $string .= '<SexeH/>';
                    //$participante->addChild('SexeH'); //ERROR DADA
                    break;
                case '1':
                    $string .= '<SexeH>1</SexeH>';
                    //$participante->addChild('SexeH', '1'); //HOME
                    break;
                case '2':
                    $string .= '<SexeH>0</SexeH>';
                    //$participante->addChild('SexeH', '0'); //DONA
                    break; 
            }

            $row[9] != '' ? $string .= '<dataNaixement>'.$row[9].'</dataNaixement>' : $string .= '<dataNaixement/>';            
            //$participante->addChild('dataNaixement', $row[9]); //dataNaixement

            //DISCAPACITAT
            switch ($row[10]) {
                case 'Si':
                    $string .= '<Discapacitat>1</Discapacitat>';
                    //$participante->addChild('Discapacitat', '1'); //SI
                    break;
                case 'No':
                    $string .= '<Discapacitat>0</Discapacitat>';
                    //$participante->addChild('Discapacitat', '0'); //NO
                    break; 
                default:
                    $string .= '<Discapacitat/>';
                    //$participante->addChild('Discapacitat'); //ERROR DADA
                    break;
            }

            $row[11] != '' ? $string .= '<Adreca>'.$row[11].'</Adreca>' : $string .= '<Adreca/>'; 
            //$participante->addChild('Adreca', $row[11]); //Adreca

            $row[12] != '' ? $string .= '<CP>'.$row[12].'</CP>' : $string .= '<CP/>'; 
            //$participante->addChild('CP', $row[12]); //CP

            $row[13] != '' ? $string .= '<CodiMunicipi>'.$row[13].'</CodiMunicipi>' : $string .= '<CodiMunicipi/>'; 
            //$participante->addChild('CodiMunicipi', $row[13]); //CodiMunicipi

            if($row[14] != ''){
                $string .= '<Telefon1>'.$row[14].'</Telefon1>';
            }
            else $string .= '<Telefon1/>'; 
            //$participante->addChild('Telefon1', $row[14]); //Telefon1

            $string .= '<Telefon2/>'; 
            //$participante->addChild('Telefon2'); //Telefon2

            $row[15] != '' ? $string .= '<Email>'.$row[15].'</Email>' : $string .= '<Email/>'; 
            //$participante->addChild('Email', $row[15]); //Email
            
            //DemandantOcupacio
            switch ($row[16]) {
                case 'Si':
                    $string .= '<DemandantOcupacio>1</DemandantOcupacio>';
                    //$participante->addChild('DemandantOcupacio', '1'); //Si
                    break;
                case 'No':
                    $string .= '<DemandantOcupacio>0</DemandantOcupacio>';
                    //$participante->addChild('DemandantOcupacio', '0'); //No
                    break; 
                default:
                    $string .= '<DemandantOcupacio/>';
                    //$participante->addChild('DemandantOcupacio'); //ERROR DADES
                    break;
            }

            $row[17] != '' ? $string .= '<Area>'.$row[17].'</Area>' : $string .= '<Area/>'; 
            //$participante->addChild('Area', $row[17]); //Area

            $row[18] != '' ? $string .= '<Categories>'.$row[18].'</Categories>' : $string .= '<Categories/>'; 
            //$participante->addChild('Categories', $row[18]); //Categories

            $row[19] != '' ? $string .= '<Estudis>'.$row[19].'</Estudis>' : $string .= '<Estudis/>'; 
            //$participante->addChild('Estudis', $row[19]); //Estudis 

            $string.='</Participants>';
        }

        $contador_participants ++;
    } 
    $string .= '</NewDataSet>'; 
    $string = "\xFF\xFE".iconv("UTF-8","UCS-2LE",$string);
    
    if($download){ 
        
        $handle = fopen("CFCP_PS20160108.xml", "w");
        fwrite($handle, $string);
        fclose($handle);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename('CFCP_PS20160108.xml'));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize('CFCP_PS20160108.xml'));
        readfile('CFCP_PS20160108.xml');
        exit;
        //header('Content-type: text/xml; charset="UTF-16"');
        //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
        //print($xml->asXML());
        
    }
    else{
        if(count($llista_errors_conforcat)>0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            foreach($llista_errors_conforcat as $key=>$value) {
                $llist_alumn .= '<tr class="danger"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';   
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="3"><h2>LLISTA DE PARTICIPANTS AMB ERROR DE DADES</h2></th>
                    </tr>
                    <tr> 
                      <th colspan="3"><h3>* Camps que <b style="color:red;">poden</b> estar buits:</h3> <h4>1. Baixa</h4> <h4>2. Telefon2</h4> <h4>3. Email</h4></th>
                    </tr>
                  </thead>
                  <thead>
                    <tr> 
                      <th>Nº PARTICIPANT</th>
                      <th>DNI ALUMNE</th>
                      <th>NOM</th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                    </div>
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                    </div>
                </div>
              </div>
            </div>';   
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            $handle = fopen("CFCP_PS20160108.xml", "w");
            fwrite($handle, $string);
            fclose($handle);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename('CFCP_PS20160108.xml'));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize('CFCP_PS20160108.xml'));
            readfile('CFCP_PS20160108.xml');
            exit;
            //header('Content-type: text/xml; charset="UTF-16"');
            //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
            //print($xml->asXML());
        }

    }

    //free result set 
    mysqli_free_result($result); 
     //close connection 
    mysqli_close($db); 
    
}

/*
* Function to get all data from specific group to create a XML
* In this case, because of the imposibility to codify to UTF-16LE the xml I dicided to create the xml
* manualy, adding the content in a string and then writting it to a file
* However, I left the old code commented
*/
function getConforcatCertificacioParticipantsXml($download, $codigrup)
{   
     //OBTENCIÓ DE LES DADES DE L´ALUMNE 
    $sqlGeneral ="SELECT DISTINCT   SUBSTRING(g.`Descripcion`, 5, 2),SUBSTRING(g.`Descripcion`, 8, 2),a.CDNICIF,a.snombre, a.sapellidos,if(mt.estado = 3, 'A','F'), cl.cnomcli,cl.cdnicif, cl.cptlcli, SUBSTRING(munCa.codi,1,5), cl.cdomicilio, clad.pyme, clad.sector, cl.cobscli, cl.CTFo1cli FROM softaula.`alumnos` AS a LEFT JOIN softaula.matriculat AS mt ON a.`CCODCLI` = mt.`CCODCLI` LEFT JOIN softaula.grupos AS g ON g.`codigogrupo` = mt.`IdGrupo` LEFT JOIN softaula.`clientes` as cl on cl.ccodcli =  mt.`FacturarA` LEFT JOIN softaula.`clientes_tab_ad` as clad on clad.ccodcli =  cl.ccodcli LEFT JOIN softaula.codigos_postales as cp on cp.`poblacion` = cl.`CPOBCLI` LEFT JOIN softaula.municipis_idscat as munCa on munCa.`nom` = cp.`poblacion` WHERE g.codigogrupo='".$codigrup."' and mt.estado != 3 ORDER BY a.sapellidos";

     //OBTENCIÓ DE LES DADES DELS FORMADORS DELS GRUPS
    $sqlFormadors ="SELECT DISTINCT p.`sNombre`,p.`sApellidos`,p.`DNI`,p.`TFO1` FROM softaula.`gruposl` AS gl LEFT JOIN softaula.`profesor` AS p ON p.`indice` = gl.`IdProfesor` WHERE gl.`CodigoGrupo` = '".$codigrup."'";

     //OBTENCIO CADENA CONNEXIÓ AMB SOFTAULA
    $db = getSoftAulaBD();

     //check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
    mysqli_set_charset($db,"utf8");    
    $resultGeneral = mysqli_query($db, $sqlGeneral);  
    $resultFormadors = mysqli_query($db, $sqlFormadors);  

    if($resultGeneral->num_rows == 0 || $resultFormadors->num_rows == 0){
        header("Location: index.php");
    }

    //LLISTA D'ERRORS ON ES GUARDA L'OBJECTE ALUMNE
    $llista_errors_participants = new ArrayObject();   
    //String on guardar els errors del grup
    $errors_grup = '' ;
    //LLISTA D'ERRORS DELS FORMADORS


    //CREACIÓ DOCUMENT XML A DINS D'UNA STRING PER DESPRÉS IMPRIMIR-LA    
    $string = '<?xml version="1.0" encoding="UTF-16LE"?><MODEL_DS15><Expedient>CP20100001</Expedient><CifEntitatExecutant>B25026428</CifEntitatExecutant><GrupAccioFormativa>';
                         
    //RECORREM CADA UNA DE LES FILES QUE ENS TORNA LA SENTENCIA SQL
    $contador_participants = 1;
    while ($row=mysqli_fetch_array($resultGeneral,MYSQLI_NUM)) 
    {    
        //echo $row[0]." -- ".$row[1]." -- ".$row[2]." -- ".$row[3]." -- ".$row[4]." -- ".$row[5]." -- ".$row[6]." -- ".$row[7]." -- ".$row[8]." -- ".$row[9]." -- ".$row[10]." -- ".$row[11]." -- ".$row[12]." -- ".$row[13]." -- ".$row[14]."<br/>"; 
        if(count($row) == 15)
        {   
            //Quan és el primer participant creem les linees que només apareixen un cop
            if($contador_participants == 1)
            {
                //Numero Acció Formativa
                $row[0] != '' ? $string .= '<NumeroAccioFormativa>'.$row[0].'</NumeroAccioFormativa>' :  $string .= '<NumeroAccioFormativa/>'; 
                //Numero Grup
                $row[1] != '' ? $string .= '<NumeroGrup>'.$row[1].'</NumeroGrup>' :  $string .= '<NumeroGrup/>'; 
                //Grup a Certificar 
                $string .= '<Grup_a_Certificar>S</Grup_a_Certificar>'; 
                //Grup Objecte de Control
                $string .= '<Grup_Objecte_de_Control>N</Grup_Objecte_de_Control>'; 
                //Generar DS15
                $string .= '<Generar_DS15>N</Generar_DS15>'; 
                //Informacio adicional
                $string .= '<Informacio_Addicional_Memoria>N</Informacio_Addicional_Memoria>'; 
            } 
                //INICI PARTICIPANTS
            $string .= '<Participants>';
            //NIF PARTICIPANT
            $row[2] != '' ? $string .= '<NifParticipant>'.$row[2].'</NifParticipant>' :  $string .= '<NifParticipant/>'; 
            //NOM PARTICIPANT
            $row[3] != '' ? $string .= '<NomParticipant>'.$row[3].'</NomParticipant>' :  $string .= '<NomParticipant/>';
            //COGNOM PARTICIPANT
            $row[4] != '' ? $string .= '<CognomsParticipant>'.$row[4].'</CognomsParticipant>' :  $string .= '<CognomsParticipant/>';
            //ESTAT PARTICIPANT
            $row[5] != '' ? $string .= '<EstatParticipant>'.$row[5].'</EstatParticipant>' :  $string .= '<EstatParticipant/>';
            //PARTICIPANT A CERTIFICAR
            $string .= '<Participant_a_Certificar>N</Participant_a_Certificar>'; 
            //Reciclatge Requalificacio
            $string .= '<ReciclatgeRequalificacio>N</ReciclatgeRequalificacio>';
            
                // DADES DE L'EMPRESA DE CADA PARTICIPANT
                $string .= '<Ocupats>';
                //RAO SOCIAL
                $row[6] != '' ? $string .= '<RaoSocial>'.$row[6].'</RaoSocial>' :  $string .= '<RaoSocial/>';
                //CifEmpresa
                $row[7] != '' ? $string .= '<CifEmpresa>'.$row[7].'</CifEmpresa>' :  $string .= '<CifEmpresa/>';
                //CodiPostalCentreTreball
                $row[8] != '' ? $string .= '<CodiPostalCentreTreball>'.$row[8].'</CodiPostalCentreTreball>' :  $string .= '<CodiPostalCentreTreball/>';
                //IdMunicipiCentreTreball
                $row[9] != '' ? $string .= '<IdMunicipiCentreTreball>'.$row[9].'</IdMunicipiCentreTreball>' :  $string .= '<IdMunicipiCentreTreball/>';
                //AdreçaCentreTreball
                $row[10] != '' ? $string .= '<AdreçaCentreTreball>'.$row[10].'</AdreçaCentreTreball>' :  $string .= '<AdreçaCentreTreball/>';
                //EsPYME
                $row[11] != '' ? $string .= '<EsPYME>'.$row[11].'</EsPYME>' :  $string .= '<EsPYME/>';
                //Sector
                $row[12] != '' ? $string .= '<Sector>'.$row[12].'</Sector>' :  $string .= '<Sector/>';
                //ReferenciaConveni
                $string .= '<ReferenciaConveni/>';
                //NumInscripcioSS
                $row[13] != '' ? $string .= '<NumInscripcioSS>'.$row[13].'</NumInscripcioSS>' :  $string .= '<NumInscripcioSS/>';
                //NombreTreballadors
                $string .= '<NombreTreballadors>0</NombreTreballadors>';
                //Telefon1
                $row[14] != '' ? $string .= '<Telefon1>'.$row[14].'</Telefon1>' :  $string .= '<Telefon1/>';
                //Email
                $string .= '<Email/>';
                //FINALITZACIÓ DADES EMPRESA
                $string .= '</Ocupats>';

            //finalització d'un participant
            $string .= '</Participants>';
        }
        $contador_participants ++;
    } 

    //AFEGIM LES DADES DELS FOMADORS
    $contador_formadors = 1; 
    while ($row2=mysqli_fetch_array($resultFormadors,MYSQLI_NUM)){
        if(count($row2) == 4){
            //INICI DELS FORMADORS
            $string .='<Formadors>';
            //NOM FORMADOR            
            $row2[0] != '' ? $string .= '<NomFormador>'.$row2[0].'</NomFormador>' :  $string .= '<NomFormador/>'; 
            //CognomsFormador            
            $row2[1] != '' ? $string .= '<CognomsFormador>'.$row2[1].'</CognomsFormador>' :  $string .= '<CognomsFormador/>'; 
            //NifFormador
            $row2[2] != '' ? $string .= '<NifFormador>'.$row2[2].'</NifFormador>' :  $string .= '<NifFormador/>'; 
            //Telefon1Formador
            $row2[3] != '' ? $string .= '<Telefon1Formador>'.$row2[3].'</Telefon1Formador>' :  $string .= '<Telefon1Formador/>'; 
            //Telefon2Formador
            $row2[3] != '' ? $string .= '<Telefon2Formador>'.$row2[3].'</Telefon2Formador>' :  $string .= '<Telefon2Formador/>';      
            //FI DELS FORMADORS
            $string .='</Formadors>';
        }
    }
    //FI GRUP ACCIÓ FORMATIVA I FINALITZACIÓ DOCUMENT
    $string .='</GrupAccioFormativa></MODEL_DS15>'; 

    $string = "\xFF\xFE".iconv("UTF-8","UCS-2LE",$string);
    
    if(!$download){ 
        
        $handle = fopen("CFCDS15_PS20160108.xml", "w");
        fwrite($handle, $string);
        fclose($handle);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename('CFCDS15_PS20160108.xml'));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize('CFCDS15_PS20160108.xml'));
        readfile('CFCDS15_PS20160108.xml');
        exit;
        //header('Content-type: text/xml; charset="UTF-16"');
        //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
        //print($xml->asXML());
        
    }
    else{
        if(count($llista_errors_conforcat)==0)
        {
            //LLISTA D'ALUMNES AMB ERRORS
            $llist_alumn=""; 

            //VARIALBE CONTADOR PER SABER EL Nº DE PARTICIAPNTS
            $index=0;

            foreach($llista_errors_conforcat as $key=>$value) {
                $llist_alumn .= '<tr class="danger"><td>Participant - '.$value->nparticipant.'</td><td>'.$value->ccodcli.'</td><td>'.$value->info.'</td></tr>';   
                $index++;
                //echo $value->ccodcli.' -- '.$value->info.'<br/>';
            }

            //LLISTA DELS ALUMNES QUE TENEN ERRORS AMB LES DADES
            $errors_content = 
            '<div class="error" id="errors-alumnes">
                 <table class="table" style="border: solid red;">
                  <thead>
                    <tr> 
                      <th colspan="3"><h2>LLISTA DE PARTICIPANTS AMB ERROR DE DADES</h2></th>
                    </tr>
                    <tr> 
                      <th colspan="3"><h3><b style="color:red;">* Camps NO OBLIGATORIS</b></h3></th> 
                    </tr>
                    <tr>
                       <th>Dades Empresa</th>
                       <th><h4>1. Sector</h4> <h4>2. ReferenciaConveni</h4> <h4>3. NumInscripcioSS</h4> <h4>4. NombreTreballadors</h4> <h4>5. Telefon1</h4> <h4>6. Telefon1</h4></th>
                    </tr>
                    <tr>
                       <th>Dades Formadors</th>
                       <th><h4>1. Sector</h4</th>
                    </tr>
                  </thead>
                  <tbody>
                  '.$llist_alumn.
                  ' 
                  </tbody>
                </table>
                <div class="row">
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" style="background: #FFCA2C;color: black;" href="index.php?download=1&codi='.$_POST["grup"].'&doc='.$_POST["tipodoc"].'" role="button">Descarregar XML</a></p>
                    </div>
                    <div class="col-md-6">
                        <p><a class="btn btn-info btn-Entrar center-block text-uppercase" href="index.php" role="button">Enrere</a></p>
                    </div>
                </div>
              </div>
            </div>';   
            //MOSTREM L'APARTAT DELS ERRORS AL HTML
            contigutErrors($errors_content);  
        }
        else{
            /*
            $handle = fopen("CFCP_PS20160108.xml", "w");
            fwrite($handle, $string);
            fclose($handle);
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename('CFCP_PS20160108.xml'));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize('CFCP_PS20160108.xml'));
            readfile('CFCP_PS20160108.xml');
            exit;
            //header('Content-type: text/xml; charset="UTF-16"');
            //header('Content-Disposition: attachment; filename="CFCP_Expedient.xml"'); 
            //print($xml->asXML());
            */
        }

    }
    /*
    //free result set 
    mysqli_free_result($result); 
     //close connection 
    mysqli_close($db); */

}
/*
* ADDED BY ALBERT
* Function to create a connexion with SOFTAULA DATA BASE
*/
function getSoftAulaBD()
{      
    $servername = "docs.ilerna.com:3306";//PER QUAN TREBALLEM EN LOCAL
    //$servername = "docs.ilerna.com";//PER QUAN TREBALLEM EN servidor
    $username = "prestashop";
    $password = "prestashop314ilerna";
    $dbname = "softaula";
    //return new mysqli($servername, $username, $password, $dbname);
    return mysqli_connect($servername, "prestashop", "prestashop314ilerna", "softaula");
}
?>
