<script>    
    // When the document is loaded...
    $(document).ready(function()
    {     
        alert("ola");
        // Validacio formulari
        $('form#contactformfree').submit(function (e) {
            
            // Guardem el valors dels camps
            var codi = $.trim($('#tipodoc').val());
            var tipdoc = $.trim($('#grup').val());            
            var correcte = true; 

            var errors = [2];
            // Comprovem que els valors siguin correctes
            if (codi  == '') {    //Quan hi ha un error al formulari             
                $("#error1").removeClass('hidden');
                //$("#name").addClass('error');     
                errors[0]="1";
            }
            else                   //Quan tot està correcte
            {                
                $("#error1").addClass('hidden');     
                //$("#name").removeClass('error');
                errors[0]="0";
            }

            if(tipdoc  == '')    //Quan hi ha un error al formulari 
            {                 
                $("#error2").removeClass('hidden');                
                //$("#email").addClass('error');  
                errors[1]="1";
            }
            else                    //Quan tot està correcte
            { 
                $("#error2").addClass('hidden');                 
                //$("#email").removeClass('error');
                errors[1]="0";
            }

            //COMPROVACIÓ DE SI HI HA ERRORS
            var isGood=true;
            for (i = 0; i < errors.length; i++) 
            { 
                if(errors[i]==1) isGood=false;
            }

            if(!isGood){            //Quan hi ha algun error al formulari
                //alert("errors");
                e.preventDefault();
                //$(document).resize();
            }
            else{                   //Quan tot està correcte
                $("input#enviar").prop('value', "Esperi sisplau...");
                $("input#enviar").prop('disabled', true);              
            }  
        }); 
    });

</script>